## Environment Prepare

Install `node_modules`:

```bash
npm install
```

or

```bash
yarn
```
### Start project

```bash
npm start
```

### Build project

```bash
npm run build
```

### Check code style

```bash
npm run lint
```

You can also use script to auto fix some lint error:

```bash
npm run lint:fix
```

### Test code

```bash
npm test
```

## Version

v1.0.4 机器列表删除按钮，添加删除确认弹框。
