const Stat = require('./stat');
const path = require('path');

const basePath = path.join(process.cwd(), 'src')

const app = new Stat(
    basePath,
    {
        ingoreDirs: ['dist', 'node_modules', '.vscode', '.umi', '.git', 'assets'],
        ingoreFiles: ['eslint.json']
    }
);
app.boot();
app.logStatDesc('统计：');