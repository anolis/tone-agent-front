import component from './zh-CN/component';
import globalHeader from './zh-CN/globalHeader';
import menu from './zh-CN/menu';
import pwa from './zh-CN/pwa';
import settingDrawer from './zh-CN/settingDrawer';
import settings from './zh-CN/settings';
import pages from './zh-CN/pages';

export default {
  'navBar.lang': '语言',
  'layout.user.link.help': '帮助',
  'layout.user.link.privacy': '隐私',
  'layout.user.link.terms': '条款',
  'app.preview.down.block': '下载此页面到本地项目',
  'app.welcome.link.fetch-blocks': '获取全部区块',
  'app.welcome.link.block-list': '基于 block 开发，快速构建标准页面',
  ...pages,
  ...globalHeader,
  ...menu,
  ...settingDrawer,
  ...settings,
  ...pwa,
  ...component,

  'Table.columns.operation': '操作',
  'Table.columns.date': '日期',
  'operation.edit': '编辑',
  'operation.delete': '删除',
  'operation.view': '查看',
  'operation.details': '详情',
  'operation.hidden': '隐藏',
  'operation.search': '搜索',
  'operation.reset': '重置',
  'operation.yes': '是',
  'operation.no': '否',
  'nothing': '无',
  'deployment.succeeded': '部署成功；',
  'deployment.failed': '部署失败；',

  'Form.input.placeholder': '请输入',
  'Form.input.message': '仅允许大小写字母、数字、下划线、中划线、点，最长50个字符',
  'Form.input.rules.message': '仅允许包含字母、数字、下划线、中划线、点，最长32个字符',
  'Form.select.placeholder': '请选择',
  'Form.select.message': '请选择',
  
  //
  'confirm.content.delete': '确定要删除吗？',
  'btn.close': '取消',
  'btn.ok': '确定',
  'btn.confirm': '确认',
  'btn.update': '更新',

};
