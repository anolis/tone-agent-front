export default {
  'component.tagSelect.expand': '展开',
  'component.tagSelect.collapse': '收起',
  'component.tagSelect.all': '全部',

  'table.total': '共',
  'table.number': '条',
};
