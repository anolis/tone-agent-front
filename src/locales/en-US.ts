import component from './en-US/component';
import globalHeader from './en-US/globalHeader';
import menu from './en-US/menu';
import pwa from './en-US/pwa';
import settingDrawer from './en-US/settingDrawer';
import settings from './en-US/settings';
import pages from './en-US/pages';

export default {
  'navBar.lang': 'Languages',
  'layout.user.link.help': 'Help',
  'layout.user.link.privacy': 'Privacy',
  'layout.user.link.terms': 'Terms',
  'app.preview.down.block': 'Download this page to your local project',
  'app.welcome.link.fetch-blocks': 'Get all block',
  'app.welcome.link.block-list': 'Quickly build standard, pages based on `block` development',
  ...globalHeader,
  ...menu,
  ...settingDrawer,
  ...settings,
  ...pwa,
  ...component,
  ...pages,

  
  //
  'Table.columns.operation': 'Operation',
  'Table.columns.date': 'Date',
  'operation.edit': 'Edit',
  'operation.delete': 'Delete',
  'operation.view': 'View',
  'operation.details': 'Details',
  'operation.hidden': 'Hidden',
  'operation.search': 'Search',
  'operation.reset': 'Reset',
  'operation.yes': 'Yes',
  'operation.no': 'No',
  'nothing': 'Nothing',
  'deployment.succeeded': 'Deployment Succeeded;',
  'deployment.failed': 'Deployment Failed;',

  'Form.input.placeholder': 'Please input',
  'Form.input.message': '仅允许大小写字母、数字、下划线、中划线、点，最长50个字符',
  'Form.input.rules.message': 'Only letters, numbers, underscores, middle dashes and dots are allowed. The maximum length is 32 characters',
  'Form.select.placeholder': 'Please select',
  'Form.select.message': 'Please select',

  //
  'confirm.content.delete': 'Are you sure you want to delete?',
  'btn.close': 'Cancel',
  'btn.ok': 'Confirm',
  'btn.confirm': 'Confirm',
  'btn.update': 'Update',

};
