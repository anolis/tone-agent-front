import React from 'react';
import { GithubOutlined } from '@ant-design/icons';
import { DefaultFooter } from '@ant-design/pro-layout';
import { useIntl, Link, history, FormattedMessage, SelectLang, useModel } from 'umi';

export default () => {
  const { formatMessage } = useIntl();
  const copy = formatMessage({ id: 'basicLayout.footer.copy' })
  
  return (
    <DefaultFooter
      copyright={`2020 ${copy}`}
      links={[
        // {
        //   key: 'Ant Design Pro',
        //   title: 'Ant Design Pro',
        //   href: 'https://pro.ant.design',
        //   blankTarget: true,
        // },
      ]}
    />
  )
};
