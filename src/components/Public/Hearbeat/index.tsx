import React, { useState, useRef, useEffect } from "react";
import { Tooltip } from "antd"
import { FormattedMessage, useIntl, getLocale } from 'umi';
import moment from 'moment';
import styles from './index.less';

const Index = ({ title, width = '100%', children }: any) => {
  const { formatMessage } = useIntl();
	const ellipsis = useRef<any>(null)
	const [show, setShow] = useState<boolean>(false)
  const [hearBeatText, setHearBeatText] = useState<string>('')

  useEffect(() => {
		isEllipsis()
	}, [hearBeatText]);

	const isEllipsis = () => {
		const clientWidth = ellipsis.current.clientWidth
		const scrollWidth = ellipsis.current.scrollWidth
		setShow(clientWidth < scrollWidth)
	};

	const en = getLocale();
  useEffect(() => {
		// case1.实现列内容心跳
		const timeId = setInterval(()=> {
			const text =  calc(title)
      setHearBeatText(text)
		}, 500)
    return () => {
      clearInterval(timeId)
    }
		// case2.中英文切换列内容也更新
	}, [en]);

  const calc = (text: string) => {
		if (!text) return text;
    const second = moment().unix() - moment(text).unix();
    if (second < 0) {
			return text;
		} else if (second < 3600) {
			return  parseInt(`${second / 60}`) + formatMessage({ id: 'hearBeat.minutes.ago'});
		} else if (second >= 3600) {
      const hours = parseInt(`${second / 3600}`);
			const minute = parseInt(`${second % 3600 / 60}`);
			if (minute === 0) {
        return hours + formatMessage({ id: 'hearBeat.hours.ago'});
			}
			return hours + formatMessage({ id: 'hearBeat.hours'}) + minute + formatMessage({ id: 'hearBeat.minutes.ago'});
		}
		return text;
	}

	return (
		show?
			<Tooltip placement="topLeft" title={hearBeatText} >
				<div ref={ellipsis} className={styles.ellipsis} style={{ width: width }}>
					{children ? children : hearBeatText||'-'}
				</div>
			</Tooltip>
      :
			<div ref={ellipsis} className={styles.ellipsis} style={{ width: width }}>
				{children ? children : hearBeatText||'-'}
			</div>
	);
};

export default Index;


