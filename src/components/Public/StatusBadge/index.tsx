import { FormattedMessage } from 'umi';
import { Badge } from 'antd';

const StatusBar = ({status}: any) => {
    let dom=null
    switch(status) {
        case 0:
           dom=<Badge status="default" text={<FormattedMessage id="Test.list.table.status.pending" />} />
           break;
        case 1:
           dom=<Badge status="processing" text={<FormattedMessage id="Test.list.table.status.running" />} />
           break;
        case 2:
           dom=<Badge status="success" text={<FormattedMessage id="Test.list.table.status.success" />} />
           break;
        case 3:
           dom=<Badge status="error" text={<FormattedMessage id="Test.list.table.status.failed" />} />
           break;
         case 4:
            dom=<Badge status="default" text={<FormattedMessage id="Test.list.table.status.STOP" />} />
            break;
         case 5:
            dom=<Badge status="default" text={<FormattedMessage id="Test.list.table.status.SKIP" />} />
            break;
        default:
   } 
   return <div style={{width:'80px'}}> {dom} </div>
};

export default StatusBar;