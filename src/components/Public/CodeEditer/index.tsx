import React, { useEffect, useState } from 'react'

import 'codemirror/lib/codemirror.css'
import 'codemirror/theme/monokai.css'
import 'codemirror/keymap/sublime'
import 'codemirror/mode/shell/shell'
import { Controlled as CodeMirror } from 'react-codemirror2'
import styles from './index.less'

export default ({ code , onChange, readOnly= false, lineNumbers= false, } : any ) => {
    const [statusCode, setStatusCode] = useState('')
    // 数据改变，组件没有重新渲染的问题。
    useEffect(()=> {
      if (code) {
        setStatusCode(code)
      }
    }, [code])

    return (
        <CodeMirror
            value={ statusCode }
            className={ styles.code_wrapper }
            options={{
                theme: 'monokai',
                keyMap: 'sublime',
                mode : 'shell',
                lineWrapping: true,
                lineNumbers: lineNumbers,
                readOnly: readOnly,
            }}
            onBeforeChange={( editor , data , value ) => onChange( value )}
        />
    )
}