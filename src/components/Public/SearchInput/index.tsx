import React,{ useState, useRef, useEffect } from 'react';
import {Space, Button, Input,Divider} from 'antd';
import { FormattedMessage, useIntl } from 'umi';

const filterRadio: React.FC<any> = ({confirm,onConfirm,autoFocus,placeholder, styleObj, currentBaseline}) => {
	const { formatMessage } = useIntl();
	const [val,setVal] = useState<any>()
	const input:any = useRef(null);
	useEffect(() => {
		input.current.focus()
	}, [autoFocus]);

	const {server_provider,test_type,id} = currentBaseline || {};
	useEffect(() => {
		if(!currentBaseline) return;
		setVal('')
	}, [server_provider,test_type,id]);

	const { Search } = Input;
	return (
		<div style={{ padding: 8 }}>
		<div>
        <Search
					ref={input}
					placeholder={placeholder || formatMessage({ id: 'Form.input.placeholder' })}
					value={val}
					onChange={(e:any) => setVal(e.target.value)}
					onSearch={(val:any) => onConfirm(val)}
					onPressEnter={() => {
						confirm()
						onConfirm(val)
					}}
					size="middle"
					style={{width:(styleObj && styleObj.container) || 150}}
        />
		</div>
		<Divider style={{marginTop:'10px',marginBottom:'4px'}} />
        <Space>
			<Button
				onClick={() => {
					confirm()
					onConfirm(val)
				}}
				type="link"
				size="small"
				style={{ width: (styleObj && styleObj.button_width) || 75 }}
			>
				<FormattedMessage id="operation.search" />
			</Button>
			<Button 
				type="text"
				onClick={() => {
					confirm()
					setVal(undefined)
					onConfirm(undefined)
				}} 
				size="small" 
				style={{ width: (styleObj && styleObj.button_width) || 75,border:'none' }}
			>
				<FormattedMessage id="operation.reset" />
			</Button>
        </Space>
      </div>
	);
};

export default filterRadio;


