import React, { useState, useEffect } from 'react';
import { Space, Button, Select, Divider, Spin, Tag } from 'antd';
import { FormattedMessage, useIntl } from 'umi';
import styles from './style.less';

const filterRadio: React.FC<any> = ({ confirm, onConfirm, autoFocus, dataSource = [] }) => {

	// const [tags, setTags] = useState<any>(['pending', 'running', 'success', 'fail', 'stop'])
	const [val, setVal] = useState<any>([])
	const { Option } = Select;
	const [focus,setFous] = useState<boolean>(false)

	return (
		<div style={{ padding: 8, width: 200 }}>
			<div className={styles.cover}
				onClick={() => {
					if(!focus){
						confirm()
					}
				}}
			>
			</div>
			<Select
				mode="multiple"
				allowClear
				notFoundContent={null}
				filterOption={false}
				showSearch
				style={{ width: '100%' }}
				onChange={(value: any) => setVal(value)}
				showArrow={false}
				autoFocus={true}
				onFocus={()=>{setFous(true) }}
				onBlur={()=> {setFous(false) }}
				value={val}
			>
				{dataSource.map((item: any) => <Option value={item} key={item}>{item}</Option> )}
			</Select>
			<Divider style={{ marginTop: '10px', marginBottom: '4px' }} />
			<Space>
				<Button
					onClick={() => {
						confirm()
						onConfirm(val)
					}}
					type="link"
					size="small"
					style={{ width: 75 }}
				>
					<FormattedMessage id="operation.search" />
			</Button>
				<Button
					type="text"
					onClick={() => {
						confirm()
						setVal(undefined)
						onConfirm(undefined)
					}}
					size="small"
					style={{ width: 75, border: 'none' }}
				>
					<FormattedMessage id="operation.reset" />
			</Button>
			</Space>
		</div>
	);
};

export default filterRadio;


