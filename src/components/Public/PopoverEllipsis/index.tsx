import React, { useState, useRef, useEffect } from "react";
import { Tooltip } from "antd"
import styles from './index.less';

const PopoverEllipsis = ({ title, width='100%', children, cornerMark, style={} }: any) => {

	const ellipsis = useRef<any>(null)
	const [show, setShow] = useState<boolean>(false)

  useEffect(() => {
		isEllipsis()
	}, [title]);

	const isEllipsis = () => {
		const clientWidth = ellipsis.current.clientWidth
		const scrollWidth = ellipsis.current.scrollWidth
		setShow(clientWidth < scrollWidth)
	};

	return (
		show?
			<Tooltip placement="topLeft" title={title} >
				<div ref={ellipsis} className={styles.ellipsis} style={{ ...style, width: width }}>
					{children ? children : title||'-'}{cornerMark}
				</div>
			</Tooltip>
      :
			<div ref={ellipsis} className={styles.ellipsis} style={{ ...style, width: width }}>
				{children ? children : title||'-'}{cornerMark}
			</div>
	);
};

export default PopoverEllipsis;


