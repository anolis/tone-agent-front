import { Table, Pagination } from 'antd';
import { useIntl } from 'umi';
import styles from './style.less';

const T_table = ({ rowKey= 'id', list = [], columns = [], total = 0, pageSize=20, loading = true, scroll = {}, onChange = () => {}, rowSelection, onRow = () => { }, current = 1 }: any) => {
  
  const { formatMessage } = useIntl();
  return <div style={{ marginTop: 4 }}>
    <Table
      dataSource={list}
      columns={columns}
      rowKey={record => record[rowKey]}
      pagination={false}
      loading={loading}
      size="small"
      rowSelection={rowSelection || undefined}
      onRow={onRow}
      scroll={scroll}
    />
    {total > 20 &&
      <div className={styles.numBox}>
        <div className={styles.total}>{formatMessage({ id: 'table.total' })}{total}{formatMessage({ id: 'table.number' })}</div>
        <Pagination
          // size="small"
          total={total}
          showSizeChanger
          showQuickJumper
          showLessItems
          current={current}
          pageSize={pageSize}
          // showTotal={total => `${formatMessage({ id: 'table.total' })} ${total}`}
          onChange={(page: number, pageSize: any) => { onChange(page, pageSize) }}
          onShowSizeChange={(page: number, pageSize: any) => { onChange(page || 1, pageSize) }}
        />
      </div>
    }
  </div>
};

export default T_table;