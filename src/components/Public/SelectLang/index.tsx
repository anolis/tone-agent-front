
import React from 'react';
// import { GlobalOutlined } from '@ant-design/icons';
import { getLocale, setLocale, FormattedMessage } from 'umi';
import styles from './index.less';

const SelectLang = (props: any) => {
  const selectedLang = getLocale();

  const changeLang = ({ key }: any) => setLocale(key);

  const locales = ['zh-CN', 'en-US'];
  const languageLabels = {
    'zh-CN': '简体中文',
    'en-US': 'English',
  };
  const languageIcons = {
    'zh-CN': '🇨🇳',
    'en-US': '🇺🇸',
  };
  
  const onClick = () =>{
    const en = getLocale() == 'zh-CN' ? 'en-US': 'zh-CN';
    // 中英文切换不加载。
    setLocale(en, false)
  }
  return (
    <div className={styles.lang} onClick={onClick} >
      <FormattedMessage id="basicLayout.lang" />
    </div>
  )
};

export default SelectLang;
