import React, { useState, useRef, useEffect } from "react";
import styles from './index.less';

const PopoverEllipsis = ({ title, height=44, children, refresh, style={ color: '#000'} }: any) => {
	const ellipsis = useRef<any>(null)
  const [show, setShow] = useState<boolean>(false); // 是否需要展开按钮；
	const [expand, setExpand] = useState<boolean>(false) // 展开按钮的状态；

  useEffect(() => {
    isEllipsis()
    // DOM变动，对DOM的监听(重要)
	}, [ellipsis.current]);
  
	const isEllipsis = () => {
    // 重新获取DOM
		const scrollHeight = ellipsis.current.scrollHeight;
    if (height < scrollHeight) {
      setShow(true)
      setExpand(false)
    } else {
      setShow(false)
      setExpand(true)
    }
	};
  const clickExpand = () => {
		setExpand(!expand)
	};

	return (
    <div style={{ marginLeft: 12 }}>
      {show ?
        <>
          <div ref={ellipsis} className={styles[expand ? 'expand' : 'ellipsis']} style={style}>{title||'-'}</div>
          <div><a onClick={clickExpand}>{expand ? '收起' : '查看全部'}</a></div>
        </>
        :
        <div ref={ellipsis} className={styles[expand ? 'expand' : 'ellipsis']} style={style}> {title||'-'}</div>
    }</div>
	);
};

export default PopoverEllipsis;


