import React, { useState, useEffect } from 'react';
import styles from './index.less';

const Index = (props) => {
  const { status, style={}, boxShadow } = props;

  return (
    <div className={styles[boxShadow ? 'PageCard' : 'PageCard']} style={style}>
      {props.children}
    </div>
  );
}

export default Index
