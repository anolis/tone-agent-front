import React, { useState, useRef, useEffect } from "react";
import { Popover } from "antd"
import styles from './index.less';

const PopoverTitle = ({ title, width='100%', children }) => {

	const ellipsis = useRef<any>(null)
	const [show, setShow] = useState<boolean>(false)

  useEffect(() => {
		isEllipsis()
	}, [title]);

	const isEllipsis = () => {
		const clientWidth = ellipsis.current.clientWidth
		const scrollWidth = ellipsis.current.scrollWidth
		setShow(clientWidth < scrollWidth)
	};

	return (
		show?
			<Popover placement="topLeft" content={title} >
				<div ref={ellipsis} className={styles.ellipsis} style={{ width: width }}>
					{children ? children : title||'-'}
				</div>
			</Popover>
      :
			<div ref={ellipsis} className={styles.ellipsis} style={{ width: width }}>
				{children ? children : title||'-'}
			</div>
	);
};

export default PopoverTitle;


