import React, { useState, useEffect } from 'react';

const style = {
  width: 1152,
  // padding: 20,
  // background: 'white',
  flexShrink: 0, // 不伸缩。
};

const ContentContainer = (props: any) => {
  // 监听当前页面宽度尺寸变化
  const [layoutHeight, setLayoutWidth] = useState(innerHeight)
  const getWindowWidth = () => setLayoutWidth(innerHeight)
  useEffect(() => {
      window.addEventListener('resize', getWindowWidth)
      return () => {
      window.removeEventListener('resize', getWindowWidth)
      }
  }, [])
  const minHeight = layoutHeight - 48 - 24 - 24

  return (
    <div className="content-container" style={{ display: 'flex', justifyContent: 'center'}}>
      <div style={{ ...style, minHeight }}>
        {props.children}
      </div>
    </div>
  );
}

export default ContentContainer
