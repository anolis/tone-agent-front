import { Tag, Space } from 'antd';
import React from 'react';
import { useModel } from 'umi';
import SelectLang from '@/components/Public/SelectLang';
import AvatarDropdown from './AvatarDropdown';
import Sign from './Sign';
import HeaderSearch from '../HeaderSearch';
import styles from './index.less';

export type SiderTheme = 'light' | 'dark';

const ENVTagColor = {
  dev: 'orange',
  test: 'green',
  pre: '#87d068',
};

const GlobalHeaderRight: React.FC = () => {
  const { initialState } = useModel('@@initialState');
  const { currentUser = {} } = initialState || {};

  if (!initialState || !initialState.settings) {
    return null;
  }

  // 判断导航栏主题色
  let className = styles.right;
  const { navTheme, layout } = initialState.settings;
  if ((navTheme === 'dark' && layout === 'top') || layout === 'mix') {
    className = `${styles.right}  ${styles.dark}`;
  }

  return (
    <Space className={className}>
      {currentUser.username ? <AvatarDropdown /> : <Sign />}
      {/* {REACT_APP_ENV && (
        <span><Tag color={ENVTagColor[REACT_APP_ENV]}>{REACT_APP_ENV}</Tag></span>
      )} */}

      {/* 国际化方案 */}
      <SelectLang className={styles.action} reload={false} />
      
      {/* 国际化方案 */}
      {/* <SelectLang className={styles.action}
        postLocalesData={()=> [
          {lang: 'en-US', label: 'English', icon: '🇺🇸', title: 'Language' }, 
          {lang: 'zh-CN',
          label: '简体中文',
          icon: '🇨🇳',
          title: '语言' }
        ]}
        
      /> */}
    </Space>
  );
};

export default GlobalHeaderRight;
