/**
 * @see https://umijs.org/zh-CN/plugins/plugin-access
 * */
export default function access(initialState: { currentUser?: API.CurrentUser | undefined }) {
  const { currentUser } = initialState || {};
  return {
    // canAdmin: currentUser && currentUser.access === 'admin', // 普通登录后可访问。
    canAdmin: !!(currentUser && currentUser.username ), // 有用户名称，就说明已登录。
  };
}
