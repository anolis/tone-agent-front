import { message, Badge } from 'antd';
import { useModel } from 'umi';

/**
 * 判断当前 string 是否是 url 格式
 * @param { string } str url string
 */
 export function isUrl(str: string) {
  try {
    const urlObj = new URL(str);
    const { href, host, origin, hostname, pathname } = urlObj;
    return href && host && origin && hostname && pathname && true
  } catch (err) {
    return false
  }
}

// 任务管理：任务状态（pending、running、success、fail、stop）
export function matchStatus(param: string) {
  if (param === 'pending') return <Badge status="default" text="Pending" />
  if (param === 'running') return <Badge status="processing" text="Running" />
  if (param === 'success') return <Badge status="success" text="Success" />
  if (param === 'fail')    return <Badge status="error" text="Fail" />
  if (param === 'stop')    return <Badge status="default" text="Stop" />
  return '-'
};

export function getLimitAuthority() {
  const { initialState, setInitialState } = useModel('@@initialState');
	const { currentUser = {} } = initialState || {};
	const limitAuthority = !!currentUser.username;
  return limitAuthority
}

// Arch的数据源
export const archData = [
  {value: 'x86_64'},
  {value: 'aarch64'},
  {value: 'riscv64'},
]
