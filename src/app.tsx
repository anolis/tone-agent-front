import React, { useState } from 'react';
import type { Settings as LayoutSettings } from '@ant-design/pro-layout';
import { PageLoading } from '@ant-design/pro-layout';
import { notification } from 'antd';
import { Link, history, useIntl } from 'umi';
import type { RequestConfig, RunTimeLayoutConfig } from 'umi';
import RightContent from '@/components/RightContent';
import Footer from '@/components/Footer';
import type { ResponseError } from 'umi-request';
import { BookOutlined, LinkOutlined } from '@ant-design/icons';
import { ReactComponent as BaselineSelected } from '@/assets/svg/Menu/Baseline-f.svg';
import { ReactComponent as BaselineNormal } from '@/assets/svg/Menu/Baseline-n.svg';
import { ReactComponent as AgentSelected } from '@/assets/svg/Menu/agent-f.svg';
import { ReactComponent as AgentNormal } from '@/assets/svg/Menu/agent.svg';
import { ReactComponent as AccountSelected } from '@/assets/svg/Menu/account-f.svg';
import { ReactComponent as AccountNormal } from '@/assets/svg/Menu/account.svg';

import { queryUserInfo as queryCurrentUser } from '@/pages/User/login/service';

// const isDev = process.env.NODE_ENV === 'development';

/** 获取用户信息比较慢的时候会展示一个 loading */
export const initialStateConfig = {
  loading: <PageLoading />,
};

/**
 * @see  https://umijs.org/zh-CN/plugins/plugin-initial-state
 * */
export async function getInitialState(): Promise<{
  settings?: Partial<LayoutSettings>;
  currentUser?: API.CurrentUser;
  fetchUserInfo?: () => Promise<API.CurrentUser | undefined>;
}> {
  const fetchUserInfo = async () => {
    try {
      const res = await queryCurrentUser() || {};
      const { data = {} } = res
      // 没有获取到用户信息就直接调到登录界面。
      if (!Object.keys(data).length) {
        history.push('/user/login');
        return res;
      }
      return data;
    } catch (error) {
      history.push('/user/login');
    }
    return undefined;
  };

  // 如果是登录页面，不执行
  if (history.location.pathname !== '/user/login') {
    const currentUser = await fetchUserInfo();
    return {
      fetchUserInfo,
      currentUser,
      settings: {},
    };
  }
  return {
    fetchUserInfo,
    settings: {},
  };
}

// 在这里自定义路由菜单icon图标
const IconRouter = ({ itemProps }: any) => {
  const { formatMessage } = useIntl();
  // 当前选中的菜单项的路由
  const aa = window.location.hash.replace('#/', '').split('/') || []
  const curRoute = aa.length ? `/${aa[0]}` : '/'
  // 自定义匹配菜单图标
  const matchMenuItemIcon = (menuItemProps: any, curRoute: string, hoverItem: string) => {
    //console.log('curRoute:', curRoute);
    const { itemPath, icon } = menuItemProps
    const iconStyle = { marginRight: 6, marginBottom: -3 }
    if (itemPath === '/task') return (curRoute === itemPath || hoverItem == itemPath) ? <BaselineSelected style={iconStyle} /> : <BaselineNormal style={iconStyle} />
    if (itemPath === '/agent') return (curRoute === itemPath || hoverItem == itemPath) ? <AgentSelected style={iconStyle} /> : <AgentNormal style={iconStyle} />
    if (itemPath === '/account') return (curRoute === itemPath || hoverItem == itemPath) ? <AccountSelected style={iconStyle} /> : <AccountNormal style={iconStyle} />
    return <span style={iconStyle}>{icon}</span>
  }
  // hover效果
  const [hoverItem, setHoverItem] = useState<string>('')
  const iconStyle = matchMenuItemIcon(itemProps, curRoute, hoverItem);

  return (
    <Link to={itemProps.itemPath}
      onMouseEnter={event => { setHoverItem(itemProps.itemPath) }} // 鼠标移入行
      onMouseLeave={event => { setHoverItem('') }}
    >
      {iconStyle}{formatMessage({ id: `${itemProps.locale}` })}
    </Link>
  )
}


// https://umijs.org/zh-CN/plugins/plugin-layout
export const layout: RunTimeLayoutConfig = ({ initialState }) => {
  return {
    rightContentRender: () => <RightContent />,
    disableContentMargin: false,
    waterMarkProps: {
      content: initialState?.currentUser?.username,
    },
    footerRender: () => <Footer />,
    onPageChange: () => {
      const { location } = history;
      // 如果没有登录，重定向到login
      if (!initialState?.currentUser?.username && location.pathname !== '/user/login') {
        history.push('/user/login');
      }
    },
    links: [],
    menuHeaderRender: (logo, title) => (
      <div id="customize_menu_header">
        <span style={{ fontFamily: 'PingFangSC-Medium', fontSize: 18 }}>{title}</span>
      </div>
    ),
    // 自定义 403 页面
    // unAccessible: <div>unAccessible</div>,
    ...initialState?.settings,
    // https://github.com/ant-design/pro-components/blob/master/packages/layout/src/layout.md
    menuItemRender: (itemProps) => {
      return (<IconRouter itemProps={itemProps} />)
    },
  };
};

const codeMessage = {
  200: '服务器成功返回请求的数据。',
  201: '新建或修改数据成功。',
  202: '一个请求已经进入后台排队（异步任务）。',
  204: '删除数据成功。',
  400: '发出的请求有错误，服务器没有进行新建或修改数据的操作。',
  401: '用户没有权限（令牌、用户名、密码错误）。',
  403: '用户得到授权，但是访问是被禁止的。',
  404: '发出的请求针对的是不存在的记录，服务器没有进行操作。',
  405: '请求方法不被允许。',
  406: '请求的格式不可得。',
  410: '请求的资源被永久删除，且不会再得到的。',
  422: '当创建一个对象时，发生一个验证错误。',
  500: '服务器发生错误，请检查服务器。',
  502: '网关错误。',
  503: '服务不可用，服务器暂时过载或维护。',
  504: '网关超时。',
};

/** 异常处理程序
 * @see https://beta-pro.ant.design/docs/request-cn
 */
const errorHandler = (error: ResponseError) => {
  const { response } = error;
  if (response && response.status) {
    const errorText = codeMessage[response.status] || response.statusText;
    const { status, url } = response;

    notification.error({
      message: `请求错误 ${status}: ${url}`,
      description: errorText,
    });
  }

  if (!response) {
    notification.error({
      description: '您的网络发生异常，无法连接服务器',
      message: '网络异常',
    });
  }
  throw error;
};

/**
 * 
 * 自定义 request配置
 * https://umijs.org/zh-CN/plugins/plugin-request
 */
export const request: RequestConfig = {
  errorHandler,
};
