
import React, { useState, useEffect, forwardRef, useImperativeHandle } from 'react';
import { FormattedMessage, useIntl } from 'umi';
import { Modal, message, Form, Select, Input, Radio, Popover } from 'antd'
import { QuestionCircleOutlined } from '@ant-design/icons'
import { matchStatus, isUrl, archData } from '@/utils/utilsHelp';
import { queryList as queryVersionList } from '../../Version/service'
import { agentAdd, agentEdit } from '../service'
import styles from './AddModal.less'

/**
 * 新增机器
 */
export default forwardRef((props: any, ref: any) => {
  const [form] = Form.useForm();
  const [loading, setLoading] = useState<any>(false)
  const [visible, setVisible] = useState(false);
  const [editData, setEditData] = useState<any>({});
  const [versionData, setVersionData] = useState<any>([]);
  const [title, setTitle] = useState('');

  // 1.请求数据
  const getVersionData = async () => {
    try {
      const res: any = await queryVersionList({ page_num: 1, page_size: 100000  });
      const { code, msg, data=[] } = res || {}
      if (code === 200) {
        setVersionData(data)
      } else {
        message.error(msg || formatMessage({id: 'request.version.fai'}) );
      }
    } catch (e) {
      // setLoading(false)
    }
  }

  useEffect(()=> {
    // 请求版本列表数据
    getVersionData()
  }, [])

  useImperativeHandle(
    ref,
    () => ({
        show: ({ title= '', detailData}: any) => {
          setVisible(true)
          // setTitle(title);
          setEditData(detailData)
          form.setFieldsValue({ ...detailData, version: detailData.version && `${detailData.version}|${detailData.os}|${detailData.arch}` })
        }
    })
  )

  const handleOk = () => {
    form.validateFields().then(async (values) => {
      setLoading(true);

      const { version, ...otherParam } = values
      const list = version.split('|')
      const field = ['version','os','arch']
      field.forEach((n: any, i: number) => {
        otherParam[n] = list[i]
      })

      const { code, msg } = values.id ? await agentEdit(otherParam) : await agentAdd(otherParam);
      if (code === 200) {
        message.success(values.id? formatMessage({id: 'modified.succeeded'}): formatMessage({id: 'created.succeeded'}));
        setVisible(false); 
        form.resetFields();
        props.callback();
      } 
      else {
        message.error(msg || formatMessage({id: 'created.failed'}));
      }
      setLoading(false);
    }).catch(() => {
      // 接口报错
      setLoading(false);
    });
  };

  const handleCancel = () => {
    setVisible(false);
    form.resetFields();
  };

  const { formatMessage } = useIntl();
  const placeholder = formatMessage({ id: "Form.select.placeholder" });
  const requiredMessage = formatMessage({ id: 'Form.select.message'});

	return (
      <div>
        <Modal title={formatMessage({ id: (editData.id ? 'agent.modal.edit.title' : 'agent.modal.add.title') }) }
          visible={visible}
          maskClosable={false}
          width={460}
          confirmLoading={loading}
          onOk={handleOk}
          onCancel={handleCancel}
        >
          <div>
          <Form form={form} layout="vertical">
            <Form.Item style={{ display: 'none'}}
              name="id">
              <Input type="hidden"/>
            </Form.Item>

            <Form.Item label="IP"
              name="ip"
              rules={[
                {
                  required: true,
                  message: formatMessage({ id: "Form.input.message" }),
                  pattern: /^[A-Za-z0-9\._-]{1,50}$/g
                }
              ]}
            >
              <Input placeholder={formatMessage({ id: "Form.input.placeholder" })} />
            </Form.Item>

            {/* <Form.Item label={<FormattedMessage id="agent.table.public_Ip" />}
              name="public_ip"
              rules={[
                {
                  required: false,
                  message: formatMessage({ id: "Form.input.message" }),
                  pattern: /^[A-Za-z0-9\._-]{1,50}$/g
                }
              ]}
            >
              <Input placeholder={formatMessage({ id: "Form.input.placeholder" })} />
            </Form.Item> */}

            <Form.Item label="SN" name="sn"
              rules={[
                {
                  required: false,
                  max: 50,
                  message: formatMessage({ id: "agent.modal.SN.message" }),
                  pattern: /^[A-Za-z0-9\._]*$/g
                }
              ]}
            >
              <Input placeholder={formatMessage({ id: "Form.input.placeholder" })}/>
            </Form.Item>

            <Form.Item label="Mode" name="mode"
              initialValue={'active'}
            >
              <Radio.Group>
                <Radio value="active">
                  active
                  <Popover placement="topLeft" content={formatMessage({ id: "mode.popover.active" })}>
                    <QuestionCircleOutlined style={{ opacity:0.65, marginLeft:4 }}/></Popover>
                </Radio>
                <Radio value="passive">
                  passive
                  <Popover placement="topLeft" content={formatMessage({ id: "mode.popover.passive" })}>
                    <QuestionCircleOutlined style={{ opacity:0.65, marginLeft:4 }}/></Popover>
                </Radio>
              </Radio.Group>
            </Form.Item>

            <Form.Item label={<FormattedMessage id="agent.modal.Version" />}
              name="version"
              rules={[{
                required: false,
                message: requiredMessage,
              }]}>
              <Select placeholder={placeholder}
                showSearch
                optionFilterProp="children"
                filterOption={(input, option:any) =>
                  option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                }>
                {versionData.map((item: any) => {
                  const tagState = item.is_new ? 'new': (item.is_stable ? 'stable': null)
                  return (
                    <Select.Option key={item.id} value={`${item.version}|${item.os}|${item.arch}`}>
                      {item.version}（{item.os} | {item.arch}）{tagState && <div className={styles.common}>{tagState}</div>}
                    </Select.Option>
                  )}
                )}
              </Select>
            </Form.Item>

            <Form.Item label="Arch"
              name="arch"
              rules={[{
                required: false,
                message: requiredMessage,
              }]}>
              <Select placeholder={placeholder}>
                {archData.map((item: any) => (
                  <Select.Option key={item.value} value={item.value}>{item.value}</Select.Option>
                ))}
              </Select>
            </Form.Item>

            <Form.Item label={<FormattedMessage id="agent.modal.Description" />}
              name="description">
              <Input.TextArea rows={4} placeholder={formatMessage({ id: "Form.input.placeholder" })} />
            </Form.Item>
          </Form>

          </div>
        </Modal>
      </div>
	);
});