
import React, { useState, useEffect, forwardRef, useImperativeHandle } from 'react';
import { Modal, message, Form, Select, Input, Radio, Popover, Spin } from 'antd'
import { QuestionCircleOutlined } from '@ant-design/icons'
import { FormattedMessage, useIntl } from 'umi';
import ShowDeployIPList from './DeployShowIPList';
import { queryList as queryVersionList } from '../../Version/service'
import { agentDeploy } from '../service'
import styles from './DeployModal.less'

/**
 * 部署Agent
 */
export default forwardRef((props: any, ref: any) => {
  const [form] = Form.useForm();
  const [loading, setLoading] = useState<any>(false)
  const [tip, setTip] = useState<any>('')
  const [visible, setVisible] = useState(false);
  // 版本数据
  const [data, setData] = useState<any>([]);
  const [selectedRow, setSelectedRow] = useState<any>([]);
  const [deployType, setDeployType] = useState('common');

  /**
   * 初始化状态数据
   * */
  const resetInitialState = () => {
    setSelectedRow([]);
    // setDeployType('common');
    setVisible(false);
  }

  // 1.请求数据
  const getVersionData = async () => {
    try {
      const res: any = await queryVersionList({ page_num: 1, page_size: 100000  });
      const { code, msg, data=[] } = res || {}
      if (code === 200) {
        setData(data)
      } else {
        message.error(msg || formatMessage({id: 'request.version.fai'}) );
      }
    } catch (e) {
      // setLoading(false)
    }
  }

  useEffect(()=> {
    if (selectedRow.length) {
      // 请求版本数据
      getVersionData()
    }
  }, [selectedRow])

  useImperativeHandle(
    ref,
    () => ({
        show: ({ title, detailData}: any) => {
          setVisible(true)
          setSelectedRow(detailData)
          // 数据回填表单控件。
          form.setFieldsValue({ 
            ids: detailData.map((item: any) => item.id)
          })
          // 只有一条数据时，的Mode、Version字段数据回填。
          if (detailData.length === 1) {
            form.setFieldsValue({
              mode: detailData[0].mode || undefined,
              version: detailData[0].version ? `${detailData[0].version}|${detailData[0].os}|${detailData[0].arch}`: undefined,
            })
          }
        }
    })
  )

  const delIP = (info: any)=> {
    setSelectedRow(info)
  }

  // const onChangeChannel = (e: any) => {
  //   setDeployType(e.target.value)
  // };

  const handleOk = () => {
    form.validateFields().then(async (values) => {
      setLoading(true);
      setTip('deploying')

      const { version, ...otherParam } = values
      const list = version.split('|')
      const field = ['version','os','arch']
      field.forEach((n: any, i: number) => {
        otherParam[n] = list[i]
      })
      // Agent部署接口
      const { code, msg, data={} } = await agentDeploy(otherParam);
      if (code === 200) {
        message.success(formatMessage({id: 'request.deploy.out'}));
        form.resetFields();
        resetInitialState()
        props.callback(data);
      } 
      else {
        message.error(msg || formatMessage({id: 'request.deploy.fai'}));
      }
      setLoading(false);
      setTip('')
    }).catch(() => {
      // 接口报错
      setLoading(false);
      setTip('')
    });
  };
  const handleCancel = () => {
    if (!loading) {
      form.resetFields();
      resetInitialState()
    }
  };

  const { formatMessage } = useIntl();
  const placeholder = formatMessage({ id: "Form.select.placeholder" });
  const requiredMessage = formatMessage({ id: 'Form.select.message'});

	return (
      <div>
        <Modal title={formatMessage({ id: 'agent.deploy.btn' }) }
          visible={visible}
          maskClosable={false}
          width={460}
          confirmLoading={loading}
          closable={!loading}
          onOk={handleOk}
          onCancel={handleCancel}
        >
          <Spin tip={tip? formatMessage({id: "request.deploying"}): ''} spinning={loading}>
          <Form form={form} layout="vertical">
            <Form.Item label="IP"
              name="ids"
              rules={[{
                required: true,
                message: formatMessage({ id: "DeployModal.ids.message" }),
              }]}>
              <ShowDeployIPList dataSource={selectedRow} delCallback={delIP}/>
            </Form.Item>

            {/* <Form.Item label={<FormattedMessage id="DeployModal.label.channel" />} name="channel"
              initialValue={'common'}>
              <Radio.Group onChange={onChangeChannel}>
                <Radio value="common"><FormattedMessage id="channel.radio.option" /></Radio>
                <Radio value="staragent">通过StarAgent部署</Radio>
              </Radio.Group>
            </Form.Item> */}

            <Form.Item label="Mode" name="mode"
              initialValue={'active'}
            >
              <Radio.Group>
                <Radio value="active">
                  active
                  <Popover placement="topLeft" content={formatMessage({ id: "mode.popover.active" })}>
                    <QuestionCircleOutlined style={{ opacity:0.65, marginLeft:4 }}/></Popover>
                </Radio>
                <Radio value="passive">
                  passive
                  <Popover placement="topLeft" content={formatMessage({ id: "mode.popover.passive" })}>
                    <QuestionCircleOutlined style={{ opacity:0.65, marginLeft:4 }}/></Popover>
                </Radio>
              </Radio.Group>
            </Form.Item>

            <Form.Item label={<FormattedMessage id="agent.modal.Version" />}
              name="version" 
              rules={[{
                required: true,
                message: requiredMessage,
              }]}>
              <Select placeholder={placeholder}
                showSearch
                optionFilterProp="children"
                filterOption={(input, option:any) =>
                  option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                }>
                {data.map((item: any) => {
                  // const { is_new, is_stable } = record
                  const tagState = item.is_new ? 'new': (item.is_stable ? 'stable': null)
                  return (
                    <Select.Option key={item.id} value={`${item.version}|${item.os}|${item.arch}`}>
                      {item.version}（{item.os} | {item.arch}）{tagState && <div className={styles.common}>{tagState}</div>}
                    </Select.Option>
                )}
                )}
              </Select>
            </Form.Item>

            <Form.Item label="User"
              name="user"
              initialValue={'root'}
              rules={[{
                required: false,
                max: 20,
                message: formatMessage({id: "user.input.message"}, {max: 20}),
              }]}>
              <Input placeholder={formatMessage({ id: "Form.input.placeholder" })} autoComplete="off" />
            </Form.Item>
            <Form.Item label="Password" name="password"
              rules={[{
                required: false,
                // message: formatMessage({ id: "Form.input.rules.message" }),
                // pattern: /^[A-Za-z0-9\._-]{1,32}$/g
              }]}>
              <Input.Password placeholder={formatMessage({ id: "Form.input.placeholder" })} />
            </Form.Item>

          </Form>
          </Spin>
        </Modal>
      </div>
	);
});