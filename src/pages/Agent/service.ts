import { request } from 'umi';

// 1. 管理
export async function queryList(
  params: {
    /** 当前的页码 */
    current?: number;
    /** 页面的容量 */
    pageSize?: number;
  },
  options?: { [key: string]: any },
) {
  return request<API.RuleList>('/v1/agent/manage', {
    method: 'GET',
    params: {
      ...params,
    },
    ...(options || {}),
  });
}

// 2. Agent部署
export const agentDeploy = async (data: any) => {
  return request('/v1/agent/deploy' , { method : 'post' , data })
}

// 3. 添加机器
export const agentAdd = async (data: any) => {
  return request('/v1/agent/manage' , { method : 'post' , data })
}
// 4. 修改机器
export const agentEdit = async (data: any) => {
  return request('/v1/agent/manage' , { 
    method : 'PUT', data 
  })
}

// 5. 删除机器
export async function agentDelete(params: any) {
  const { id } = params
  return request(`/v1/agent/manage`, {
    method: 'DELETE',
    params: {
      id,
    },
  });
}

// 心跳测试
export async function agentHeartbeat(params: any) {
  return request(`/v1/agent/heartbeat`, {
    method: 'GET',
    params,
  });
}
