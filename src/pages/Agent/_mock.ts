export default {
  //
  'GET /v1/agent/manage': (req: Request, res: Response) => {
    res.json(
      {
        "code": 200,
        "total": 3676,
        "total_page": 246,
        "page_num": 1,
        "page_size": 15,
        "previous": null,
        "next": "/v1/agent/manage?page_num=2",
        "data": [
          {
            "ip": "121.40.98.147",
            public_ip: '181.711.98.199',
            "status": "running",
            "sync": false,
            "result": null,
            "error_msg": null,
            "stop_tid": null,
            "task_pid": "96316",
            "in_schedule": true,
            "id": 14962
          },
        ]
      }
    )
  },
  
}