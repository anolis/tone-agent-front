import { FormattedMessage, useIntl, history } from 'umi';
import React, { useCallback, useState, useEffect, useRef } from 'react';
import { Space, Button, message, Alert } from 'antd'
import { PageContainer, PageCard } from '@/components/Public'
import { getLimitAuthority } from '@/utils/utilsHelp';
import AddModal from './Module/AddModal'
import DeployModal from './Module/DeployModal'
import AgentList from './AgentList';
import { queryList, agentDelete } from './service';
import styles from './index.less'

/** 机器管理 */
const List = (props: any) => {
  const { formatMessage } = useIntl()
  // 权限
	const limitAuthority = getLimitAuthority();
  const [loading, setLoading] = useState<any>(false)
  const [data, setData] = useState<any>({ data: [], total: 0, page_num: 1, page_size: 20 })
  const [filterQuery, setFilterQuery] = useState<any>({}); // 过滤参数。
  const detailsModal: any = useRef(null)
  const deployModal: any = useRef(null)
  const [selectedRow, setSelectedRow] = useState<any>([]); // 选中的行
  const [selectedRowId, setSelectedRowId] = useState<any>([]); // 选中的行
  const [deployResult, setDeployResult] = useState<any>({
    success_servers: [], 
    fail_servers: [], // [{ip: '111.2322.2342', msg: 'dfefefe'}, {ip: '111.2322.2342', msg: 'dfefefe'},{ip: '111.2322.2342', msg: 'dfefefe'}] 
  }); // Agent部署的结果提示

  // 1.请求数据
  const getTableData = async (query: any) => {
    setLoading(true)
    try {
      const res: any = await queryList({ ...filterQuery, page_size: data.page_size, ...query });
      const { code, msg } = res || {}
      if (code === 200) {
        setData(res)
      } else {
        message.error(msg || formatMessage({ id: 'request.data.failed'}) );
      }
      setLoading(false)
    } catch (e) {
      setLoading(false)
    }
  }

	useEffect(() => {
    // getTableData({ page_num: 1, page_size: 15 })
	}, [])

  // 分页
	const onChange = (page:number, pageSize:number) => {
    // console.log('onChange:')
    getTableData({ page_num: page, page_size: pageSize })
  }

  // 新增机器
  const addOrEditCallBack = async (type: string, info: any) => {
    // console.log('data:', type, info)
    if (type === 'add') {
      detailsModal.current?.show({ detailData: {} });
    } else if (type === 'edit') {
      detailsModal.current?.show({ detailData: info });
    } else if (type === 'delete') {
      deleteAgentClick(info)
    } else if (type === 'heartbeat') {
      getTableData({ page_num: data.page_num, page_size: data.page_size })
    }
  }

  // 新增回调
  const addCallback = ()=> {
    getTableData({ page_num: data.page_num, page_size: data.page_size })
  }
  
  // 删除
  const deleteAgentClick = async (query: any) => {
    const res = await agentDelete(query);
    if (res.code === 200) {
      message.success(formatMessage({ id: 'deletion.succeeded'}) );
      getTableData({ page_num: data.page_num, page_size: data.page_size })
    } else {
      message.error(res.msg || formatMessage({ id: 'request.data.failed'}));
    }
  }
  
  // 部署
  const deployClick = () => {
    deployModal.current?.show({ detailData: selectedRow });
  }
  // 部署回调
  const deployCallback = (info: any) => {
    // case1. 部署结果信息
    const { success_servers= [], fail_servers= []} = info;
    setDeployResult({ success_servers, fail_servers })
    // case2. 清除已选项
    selectedRow([])
    selectedRowId([])
    // case3. 刷新表格数据
    getTableData({ page_num: data.page_num, page_size: data.page_size })
  }
  // Alert的提示内容
  const deployReactNode = ()=> {
    const { success_servers= [], fail_servers= [] } = deployResult
    const successIps = success_servers.map((item: any)=> item.ip).join(' / ')
    // const failIps = fail_servers.map((item: any)=> item.ip).join(' / ')

    return (
      <div>
        {success_servers.length ? (<div><span style={{ color: '#1890FF'}}>{successIps}</span>&nbsp;<FormattedMessage id="deployment.succeeded" />&nbsp;</div>) : null }
        {!!fail_servers.length && (
          <>
            {fail_servers.map((item: any)=> <div><span style={{ color: '#F5222D'}}>{item.ip}</span>&nbsp;{item.msg || <FormattedMessage id="deployment.failed" />}</div>)}
          </>)
        }
      </div>
    )
  }
  // 关闭Alert
  const closeAlert = () => {
    setDeployResult({})
  }

  // 复选
  const onSelectChange = (selectedRowKeys: any, selectedRows: any) => {
    setSelectedRowId(selectedRowKeys)
    setSelectedRow(selectedRows)
  }

  // 过滤查询
  const filterCallBack= (info: any) => {
    // console.log('info:', info)
    const query = { ...info }
    setFilterQuery(query)
    getTableData(query)
  }

	return (
    <PageContainer>
      <PageCard>
        <div>
          {/* <div className={styles.Alert} >
            <Alert message={`11.160.210.51 / 11.160.210.51/11.160.210.51 / 11.160.210.51/ 11.160.210.51/11.160.210.51 / 11.160.210.51/ 11.160.210.51/11.160.210.51 / 
  11.160.210.51 / 11.160.210.51/11.160.210.51 / 11.160.210.51/ 11.160.210.51/11.160.210.51 / 11.160.210.51/ 11.160.210.51/11.160.210.51 
  11.160.210.51 / 11.160.210.51/11.160.210.51 `} type="info" showIcon closable onClose={closeAlert} />
          </div> */}
          {( (deployResult?.success_servers?.length) || (deployResult?.fail_servers?.length) ) ?
            <div className={styles.Alert}>
              <Alert message={deployReactNode()} type="info" showIcon closable onClose={closeAlert} /> 
            </div>
            : null
          }
          
          <div className={styles.header}>
            <span className={styles.title}><FormattedMessage id="agent.list" /></span>
            {limitAuthority ?
              <Space>
                <Button onClick={deployClick} type="primary" disabled={!selectedRow.length}><FormattedMessage id="agent.deploy.btn" /></Button>
                <Button onClick={()=> addOrEditCallBack('add', {})} type="primary"><FormattedMessage id="agent.modal.add.title" /></Button>
              </Space>
              : null
            }
          </div>
          <AgentList
            data={data}
            onChange={onChange}
            // onRow={onRow}
            loading={loading}
            filterCallBack={filterCallBack}
            editCallBack={addOrEditCallBack}
            rowSelectionCallBack={{ selectedRowKeys: selectedRowId, onSelectChange: onSelectChange }}
          />

          <AddModal ref={detailsModal} callback={addCallback} />
          <DeployModal ref={deployModal} callback={deployCallback} />
        </div>
      </PageCard>
    </PageContainer>
	);
};

export default List;
