import React, { useState, useEffect } from 'react';
import { Popover, Tooltip, Space, Badge, message, Spin } from 'antd';
import { FormattedMessage } from 'umi';
import { agentHeartbeat } from '../service';

export default ({ callBack, record }) => {
  const [heartbeatLoading, setHeartbeatLoading] = useState<any>(false)

  const heartbeat = async (info)=> {
    setHeartbeatLoading(true)
    const res = await agentHeartbeat({ id: info.id}).catch(()=> setHeartbeatLoading(false))
    setHeartbeatLoading(false)
    if (res?.code === 200) {
      const reactNode = res?.data?.info?.split('\n')?.map((item: any)=> <div key={item} style={{ textAlign: 'left'}}>{item}</div> )
      if (res.data.heartbeat === 'ok') {
        message.success(<span>ok {reactNode}</span>);
        callBack('heartbeat', {})
      } else if (res.data.heartbeat === 'fail') {
        message.error(<span>fail {reactNode}</span>);
      }
    } else {
      message.error(res.msg || formatMessage({ id: 'request.data.failed'}) );
    }
  }

  return (
    <Spin spinning={heartbeatLoading}>
      <a><span onClick={()=> heartbeat(record)}><FormattedMessage id="agent.table.heartbeat.test" /></span></a>
    </Spin>
  )

}