import React, { useState, useEffect } from 'react';
import { Popover, Tooltip, Space, Badge, Popconfirm, Spin } from 'antd';
import { QuestionCircleOutlined, FilterFilled } from '@ant-design/icons'
import { FormattedMessage, useIntl } from 'umi';
import CommonTable from '@/components/Public/CommonTable';
import { PopoverEllipsis, HearBeat } from '@/components/Public';
import SearchInput from '@/components/Public/SearchInput';
import SelectStatus from '@/components/Public/SelectStatus';
import { getLimitAuthority } from '@/utils/utilsHelp';
import HeartbeatBtn from './HeartbeatBtn';
import styles from './style.less';

const ProductList = ({ data, loading, onChange, onRow, filterCallBack, editCallBack, rowSelectionCallBack= {} }:any) => {
	const { formatMessage } = useIntl();
	const { selectedRowKeys = [], onSelectChange } = rowSelectionCallBack
	//
  const [autoFocus, setFocus] = useState<boolean>(true)
	const [ip, setIp] = useState<string>()
	const [publicIp, setPublicIp] = useState<string>()
	const [status, setStatusEnum] = useState<any>()
	const [sn, setSn] = useState<string>()
	const [tsn, setTsn] = useState<string>()
	const [mode, setModeEnum] = useState<any>()
	const [version, setVersion] = useState<string>()
	const [arch, setArch] = useState<string>()
	// 权限
	const limitAuthority = getLimitAuthority();

  const AgentStatus = ({ str }: any) => {
   if (str === 'online') {
      return <Badge status="success" text="online" />
	 } else if (str === 'offline') {
		  return <Badge status="error" text="offline" />
	 }
	 return <span>-</span>
  }

	let columns: any = [
		{
			title: <FormattedMessage id="agent.table.IP" />,
			dataIndex: 'ip',
			onCell: () => ({ style: { maxWidth: 150 } }),
			render: (text:any) => {
				return (
				<span className={styles.ellipsis}>{text || '-'}
				  {/* <DeployGoing style={{ marginLeft: 3}}/> */}
				</span>)
			},
			filterIcon: () => <FilterFilled style={{ color: ip ? '#1890ff' : undefined }} />,
			filterDropdown: ({ confirm }: any) => <SearchInput confirm={confirm} autoFocus={autoFocus} onConfirm={(val: string) => { setIp(val) }} />,
			onFilterDropdownVisibleChange: (visible: any) => {
				if (visible) {
						setFocus(!autoFocus)
				}
		  },
		},
		// {
		//   title: <FormattedMessage id="agent.table.public_Ip" />,
		// 	dataIndex: 'public_ip',
		// 	onCell: () => ({ style: { maxWidth: 150 } }),
		// 	render: (text:any) => {
		// 	return (
		// 	  <span className={styles.ellipsis}>{text || '-'}
		// 			{/* <DeployGoing style={{ marginLeft: 3}}/> */}
		// 		</span>)
		// 	},
		// 	filterIcon: () => <FilterFilled style={{ color: publicIp ? '#1890ff' : undefined }} />,
		// 	filterDropdown: ({ confirm }: any) => <SearchInput confirm={confirm} autoFocus={autoFocus} onConfirm={(val: string) => { setPublicIp(val) }} />,
		// 	onFilterDropdownVisibleChange: (visible: any) => {
		// 		if (visible) {
		// 				setFocus(!autoFocus)
		// 		}
		// 	},
		// },
		{
			title: <FormattedMessage id="agent.table.TSN" />,
			dataIndex: 'tsn',
      render: (text:any) => {
				return <PopoverEllipsis title={text} width={140} />
			},
			filterIcon: () => <FilterFilled style={{ color: tsn ? '#1890ff' : undefined }} />,
			filterDropdown: ({ confirm }: any) => <SearchInput confirm={confirm} autoFocus={autoFocus} onConfirm={(val: string) => { setTsn(val) }} />,
			onFilterDropdownVisibleChange: (visible: any) => {
				if (visible) {
						setFocus(!autoFocus)
				}
		  },
		},

		{
			title: <FormattedMessage id="agent.table.Status" />,
			dataIndex: 'status',
			onCell: () => ({ style: { minWidth: 80 } }),
			render: (text:any) => <AgentStatus str={text} />,
			filterIcon: () => <FilterFilled style={{ color: status && status.length > 0 ? '#1890ff' : undefined }} />,
			filterDropdown: ({ confirm }: any) =>
			  <SelectStatus confirm={confirm} onConfirm={(val: string) => { setStatusEnum(val) }}
				  dataSource={['online', 'offline']} />,
		},
		{
			title:
				(
					<span style={{ padding: 0, overflow: 'unset' }}>
						<FormattedMessage id="agent.table.Mode" />
						<Tooltip title={<FormattedMessage id="agent.table.Mode.Tooltip" /> }>
							<QuestionCircleOutlined style={{ opacity:0.65,marginLeft:3 }}/>
						</Tooltip>
					</span>
				),
			className: "agent-table-mode-style",
			dataIndex: 'mode',
			onCell: () => ({ style: { minWidth: 90 } }),
			render: (text:any) => {
				return <span className={styles.ellipsis}>{text || '-'}</span>
			},
			filterIcon: () => <FilterFilled style={{ color: mode && mode.length > 0 ? '#1890ff' : undefined }} />,
			filterDropdown: ({ confirm }: any) =>
			  <SelectStatus confirm={confirm} onConfirm={(val: string) => { setModeEnum(val) }}
				  dataSource={['active', 'passive']} />,
		},
    {
			title: <FormattedMessage id="agent.table.Version" />,
			dataIndex: 'version',
			render: (text:any) => {
				return <PopoverEllipsis title={text} width={100} />
			},
			filterIcon: () => <FilterFilled style={{ color: version ? '#1890ff' : undefined }} />,
			filterDropdown: ({ confirm }: any) => <SearchInput confirm={confirm} autoFocus={autoFocus} onConfirm={(val: string) => { setVersion(val) }} />,
			onFilterDropdownVisibleChange: (visible: any) => {
				if (visible) {
						setFocus(!autoFocus)
				}
		  },
		},
    {
			title: <FormattedMessage id="agent.table.Arch" />,
			dataIndex: 'arch',
			onCell: () => ({ style: { maxWidth: 80 } }),
			render: (text:any) => {
				return <PopoverEllipsis title={text} width={80} />
			},
			filterIcon: () => <FilterFilled style={{ color: arch ? '#1890ff' : undefined }} />,
			filterDropdown: ({ confirm }: any) => <SearchInput confirm={confirm} autoFocus={autoFocus} onConfirm={(val: string) => { setArch(val) }} />,
			onFilterDropdownVisibleChange: (visible: any) => {
				if (visible) {
						setFocus(!autoFocus)
				}
			},
		},
    {
			title: <FormattedMessage id="agent.table.Heartbeat" />,
			dataIndex: 'heartbeat',
			onCell: () => ({ style: { maxWidth: 100 } }),
			render: (text:any) => {
				return <HearBeat title={text} width={100} />
			}
		},
		{
			title: <FormattedMessage id="agent.table.description" />,
			dataIndex: 'description',
			onCell: () => ({ style: { maxWidth: 100 } }),
			render: (text:any) => {
				return <PopoverEllipsis title={text} width={100} />
			}
		},
	]
	if (limitAuthority) {
    columns = columns.concat([
    {
      title: <FormattedMessage id="Table.columns.operation" />,
      onCell: () => ({ style: { minWidth: 160 } }),
      align: 'center',
      render: (text: any, record: any) => {
          return (<div>
							<Space>
							  <HeartbeatBtn callBack={editCallBack} record={record} />
								<a><span onClick={()=> editCallBack('edit', record)}><FormattedMessage id="operation.edit" /></span></a>
								<Popconfirm
									placement="bottomRight"
									title={formatMessage({ id: 'confirm.content.delete' }) }
									onConfirm={()=> editCallBack('delete', record) }
									okText={formatMessage({ id: 'btn.ok' }) }
									cancelText={formatMessage({ id: 'btn.close' }) }
								>
									<a><span><FormattedMessage id="operation.delete" /></span></a>
                </Popconfirm>
							</Space>
            </div>
          )
      },
    },
		])
	}

	// 行选 && 权限
	const rowSelections = (rowSelectionCallBack && limitAuthority) ? {
		selectedRowKeys: selectedRowKeys,
		onChange: (selectedRowKeys:any, selectedRows: any)=> {
			onSelectChange(selectedRowKeys, selectedRows)
		},
	} : undefined;

	useEffect(() => {
		const params = { page_num: 1, ip, status, sn, tsn, mode, version, arch }
		filterCallBack(params)
  }, [ip, status, sn, tsn, mode, version, arch]);

	let list = data.data, total = data.total, current = data.page_num, pageSize = data.page_size
	return (
		<div className={styles.AgentList}>
			<CommonTable
				rowKey= 'id'
				columns={columns}
				total={total}
				pageSize={pageSize}
				current={current}
				list={list}
				loading={loading}
				onRow={onRow}
				onChange={onChange}
				rowSelection={rowSelections}
			/>
		</div>
	)
};

export default ProductList;
