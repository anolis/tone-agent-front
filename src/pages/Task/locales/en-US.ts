export default {
  'task.list': 'Task List',

  'task.table.TID': 'TID',
  'task.table.Status': 'Status',
  'task.table.IP': 'IP',
  'task.table.TSN': 'TSN',
  'task.table.Sync': 'Sync',
  'task.table.Result': 'Result',
  'task.table.StartTime': 'Start time',
  'task.table.FinishTime': 'Finish time',
  'task.details': 'Task Details',
  'task.info': 'Task Information',
  'task.status': 'Task Status',
  'task.script': 'Task Script',
  'task.result': 'Task Result',
}