export default {
  'task.list': '任务列表',

  'task.table.TID': 'TID',
  'task.table.Status': '状态',
  'task.table.IP': 'IP',
  'task.table.TSN': 'TSN',
  'task.table.Sync': 'Sync',
  'task.table.Result': '结果',
  'task.table.StartTime': '开始时间',
  'task.table.FinishTime': '结束时间',
  'task.details': '任务详情',
  'task.info': '任务信息',
  'task.status': '任务状态',
  'task.script': '任务脚本',
  'task.result': '任务结果',
}