
import React, { useCallback, useState, useEffect, forwardRef, useImperativeHandle } from 'react';
import { FormattedMessage, useIntl, history } from 'umi';
import moment from 'moment';
import { Modal, message, Row } from 'antd'
import CodeEditer from '@/components/Public/CodeEditer';
import TextEllipsis from '@/components/Public/TextEllipsis';
import { matchStatus, isUrl } from '@/utils/utilsHelp';
import { queryTaskList } from '../service'
import styles from './DetailsModal.less'

/**
 * 任务详情对话框
 */
export default forwardRef((props: any, ref: any) => {
  const { formatMessage } = useIntl()
  const [loading, setLoading] = useState<any>(false)
  const [visible, setVisible] = useState(false);
  const [data, setData] = useState<any>({});
  const [title, setTitle] = useState('');
  const [current, setCurrent] = useState<any>({})
  // console.log('data:', data)

  // 1.请求数据
  const getTableData = async (query: any) => {
    setLoading(true)
    try {
      const res: any = await queryTaskList(query) || {};
      const { code, msg } = res
      if (code === 200) {
        setData(res)
      } else {
        message.error(msg || formatMessage({ id: 'request.data.failed'}) );
      }
      setLoading(false)
    } catch (e) {
      setLoading(false)
    }
  }

  useImperativeHandle(
    ref,
    () => ({
        show: ({ title= '', item}: any) => {
          setVisible(true)
          // setTitle(title);
          setData(item)
        }
    })
  )

  const handleOk = () => {
    setVisible(false);
  };
  const handleCancel = () => {
    setVisible(false);
  };

  const ItemRow = ({label, text, type, children }: any) => {
    return (
      <div className={styles.ItemRow}>
        <div className={styles.label}>{label}</div>
        <div className={`${styles.values} ${styles[type]}`}>
          {children ? children :
            isUrl(text) ? (<a target="__blank" href={text}>{text}</a>) :  (
              <span className={styles.text}>{text||'-'}</span>
            )
          }
        </div>
      </div>
    )
  }

  const ItemRow2 = ({label, text='', type, children,  style={} }: any) => {
    // 处理字符串换行
    const key = ' 1/';
    const tempStr = text ? text.replace(new RegExp(key, 'g'), "\n1/") : '';
    // 逗号，空格，等转换成 换行
    const restStr = tempStr ? tempStr.replace(/[\ |\,|\，]/g,'\n') : '-';
    return (
      <div className={styles.ItemRow}>
        <div className={styles.label}>{label}</div>
        <TextEllipsis title={restStr} height={44} style={style}/>
      </div>
    )
  }
  const jsonData = JSON.stringify(data) || '';
  const formatResult = JSON.stringify(JSON.parse(jsonData),null, 4); //格式化后的 json字符串形式
  //console.log( formatResult )

	return (
      <div>
        <Modal title={<FormattedMessage id="task.details" />}
          visible={visible}
          maskClosable={true}
          width={900}
          confirmLoading={loading}
          onOk={handleOk}
          onCancel={handleCancel}
        >
          <div>
            <Row className={styles.form_row}>
              <div className={styles.page_body_nav}>
                <span><FormattedMessage id="task.info" /></span>
              </div>
              <div className={styles.content}>
                <ItemRow label="TID" text={data.tid} />
                <ItemRow label="Sync" text={data.sync ? 'true' : 'false'} />
                <ItemRow2 label="Env" text={data.env} style={{ whiteSpace: 'pre-wrap' }}/>
                <ItemRow label="Script type" text={data.script_type || formatMessage({id: 'nothing'})} />
                <ItemRow2 label="Args" text={data.args} style={{ whiteSpace: 'pre-wrap' }}/>
                <ItemRow label="Cwd" text={data.cwd} />
                <ItemRow label="Timeout" text={data.timeout} />
                <ItemRow label="IP" text={data.ip} />
                <ItemRow label="SN" text={data.sn} />
                <ItemRow label="TSN" text={data.tsn} />
                <ItemRow label="Access key" text={data.access_key} />
              </div>
            </Row>

            <Row className={styles.form_row}>
              <div className={styles.page_body_nav}>
                <span><FormattedMessage id="task.status" /></span>
              </div>
              <div className={styles.content}>
                <ItemRow label="Status">{matchStatus(data.status)}</ItemRow>
                <ItemRow label="Delivery time" text={data.delivery_time ? moment(data.delivery_time).format('YYYY-MM-DD HH:mm:ss') : '-'} />
                <ItemRow label="Start time" text={data.start_time ? moment(data.start_time).format('YYYY-MM-DD HH:mm:ss') : '-'} />
                <ItemRow label="Finish time" text={data.finish_time ? moment(data.finish_time).format('YYYY-MM-DD HH:mm:ss') : '-'} />
                <ItemRow label="Exit code" text={data.exit_code} />
                <ItemRow label="Error code" text={data.error_code} />
                <ItemRow label="Error msg" text={data.error_msg} />
                <ItemRow label="Task pid" text={data.task_pid} />
                <ItemRow label="Stop tid" text={data.stop_tid} />
                <ItemRow label="In schedule" text={data.in_schedule ? 'yes': 'no'} />
              </div>
            </Row>

            <Row className={styles.form_row}>
              <div className={styles.page_body_nav}>
                <span><FormattedMessage id="task.script" /></span>
              </div>
              {data.script ?
                <div style={{ width: '100%', marginTop: 16, marginBottom: 16  }}>
                  <CodeEditer code={data.script} lineNumbers
                    // readOnly="nocursor"
                  />
                </div>
              :
                <div className={styles.content}>
                  <ItemRow2 label="Script" text={data.script} />
                </div>
              }
            </Row>

            <Row className={styles.form_row}>
              <div className={styles.page_body_nav}>
                <span><FormattedMessage id="task.result" /></span>
              </div>
              {data.result ?
                <div style={{ width: '100%', marginTop: 16, marginBottom: 16  }}>
                  <CodeEditer code={data.result} // readOnly="nocursor" 
                  />
                </div>
                :
                <div className={styles.content}>
                  <ItemRow2 label="Result" text={data.result} />
                </div>
              }
            </Row>

            <Row className={styles.form_row}>
              <div className={styles.page_body_nav}>
                <span>JSON</span>
              </div>
              <div style={{ width: '100%', marginTop: 17 }}>
                <CodeEditer 
                  code={formatResult }
                  // readOnly="nocursor"
                />
              </div>
            </Row>
          </div>
        </Modal>
      </div>
	);
});