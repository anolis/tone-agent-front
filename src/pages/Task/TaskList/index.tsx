import React, { useState, useEffect } from 'react';
import { Popover, Tooltip, Space } from 'antd';
import { FilterFilled, QuestionCircleOutlined } from '@ant-design/icons';
import { FormattedMessage, useModel } from 'umi';
import moment from 'moment';
import CommonTable from '@/components/Public/CommonTable';
import PopoverEllipsis from '@/components/Public/PopoverEllipsis';
import SearchInput from '@/components/Public/SearchInput';
import SelectStatus from '@/components/Public/SelectStatus';
import { matchStatus, getLimitAuthority } from '@/utils/utilsHelp';
import styles from './style.less';

const ProductList = ({ data, loading, onChange, filterCallBack, detailCallBack,  onRow }:any) => {
  // 权限
	const limitAuthority = getLimitAuthority();
  
	const [autoFocus, setFocus] = useState<boolean>(true)
	const [tid, setTid] = useState<string>()
	const [status, setStatusEnum] = useState<any>()
	const [ip, setIp] = useState<string>()

	let columns: any = [
		{
			title: <FormattedMessage id="task.table.TID" />,
			dataIndex: 'tid',
			fixed: 'left',
			width: 'auto',
			filterIcon: () => <FilterFilled style={{ color: tid ? '#1890ff' : undefined }} />,
			filterDropdown: ({ confirm }: any) => <SearchInput confirm={confirm} autoFocus={autoFocus} onConfirm={(val: string) => { setTid(val) }} />,
			onFilterDropdownVisibleChange: (visible: any) => {
				if (visible) {
						setFocus(!autoFocus)
				}
		  },
      render: (text:any) => {
				return <PopoverEllipsis title={text} width={218} />
			},
		},
		{
			title: <FormattedMessage id="task.table.Status" />,
			dataIndex: 'status',
			onCell: () => ({ style: { minWidth: 90 } }),
			render: (text:any) => {
				return <span>{matchStatus(text)}</span>
			},
			filterIcon: () => <FilterFilled style={{ color: status && status.length > 0 ? '#1890ff' : undefined }} />,
			filterDropdown: ({ confirm }: any) => 
			  <SelectStatus confirm={confirm} onConfirm={(val: string) => { setStatusEnum(val) }}
				  dataSource={['pending', 'running', 'success', 'fail', 'stop']} />,
		},
		{
			title: <FormattedMessage id="task.table.IP" />,
			dataIndex: 'ip',
			onCell: () => ({ style: { maxWidth: 130 } }),
			render: (text:any) => {
				return <span className={styles.ellipsis}>{text || '-'}</span>
			},
			filterIcon: () => <FilterFilled style={{ color: ip ? '#1890ff' : undefined }} />,
			filterDropdown: ({ confirm }: any) => <SearchInput confirm={confirm} autoFocus={autoFocus} onConfirm={(val: string) => { setIp(val) }} />,
			onFilterDropdownVisibleChange: (visible: any) => {
				if (visible) {
						setFocus(!autoFocus)
				}
		  },
		},

    {
			title: <FormattedMessage id="task.table.TSN" />,
			dataIndex: 'tsn',
			render: (text:any) => {
				return <PopoverEllipsis title={text} width={120} />
			}
		},
    {
			title: <FormattedMessage id="task.table.Sync" />,
			dataIndex: 'sync',
			onCell: () => ({ style: { maxWidth: 100 } }),
			render: (text:any) => {
				// '同步' : '异步'
				return <span>{text ? 'true' : 'false' }</span>
			}
		},
    {
			title: <FormattedMessage id="task.table.Result" />,
			dataIndex: 'result',
			onCell: () => ({ style: { maxWidth: 100 } }),
			render: (text:any) => {
				return <PopoverEllipsis title={text} width={100} />
			}
		},
    {
			title: <FormattedMessage id="task.table.StartTime" />,
			dataIndex: 'start_time',
			render: (text:any) => {
				return <PopoverEllipsis title={text ? moment(text).format('YYYY-MM-DD HH:mm:ss') : '-'} width={140} />
			}
		},
    {
			title: <FormattedMessage id="task.table.FinishTime" />,
			dataIndex: 'finish_time',
			render: (text:any) => {
				return <PopoverEllipsis title={text ? moment(text).format('YYYY-MM-DD HH:mm:ss') : '-'} width={140} />
			}
		},
    {
			title: <FormattedMessage id="Table.columns.operation" />,
			onCell: () => ({ style: { maxWidth: 90 } }),
			align: 'center',
			render: (text: any, record: any) => {
					return (<div>
							<Space>
								<a><span onClick={()=> detailCallBack(record)}><FormattedMessage id="operation.details" /></span></a>
							</Space>
						</div>
					)
			},
		}
	];
	// if (limitAuthority) {
  //   columns = columns.concat([
	// 	])
	// }

	useEffect(() => {
		const params = { page_num: 1, tid, status, ip }
		filterCallBack(params)
  }, [tid, status, ip]);

	let list = data.data, total = data.total, current = data.page_num, pageSize = data.page_size
	return <CommonTable
		columns={columns}
		total={total}
		pageSize={pageSize}
		current={current}
		list={list}
		loading={loading}
		onRow={onRow}
		onChange={onChange}
	/>
};

export default ProductList;