import { request } from 'umi';

// 1. 任务管理
export async function queryTaskList(
  params: {
    /** 当前的页码 */
    current?: number;
    /** 页面的容量 */
    pageSize?: number;
  },
  options?: { [key: string]: any },
) {
  return request<API.RuleList>('/v1/task/manage', {
    method: 'GET',
    params: {
      ...params,
    },
    ...(options || {}),
  });
}

