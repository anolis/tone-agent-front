import { FormattedMessage, useIntl, history } from 'umi';
import React, { useCallback, useState, useEffect, useRef } from 'react';
import { message } from 'antd'
import { PageContainer, PageCard } from '@/components/Public'
import TaskList from './TaskList';
import { queryTaskList } from './service'
import DetailsModal from './Module/DetailsModal';

const List = (props: any) => {
  const { formatMessage } = useIntl()
  const [loading, setLoading] = useState<any>(false)
  const [data, setData] = useState<any>({ data: [], total: 0, page: 1 })
  const [pageSize, setPageSize] = useState<number>(20)
  const [filterQuery, setFilterQuery] = useState<any>({})
  const detailsModal: any = useRef(null)

  // 1.请求数据
  const getTableData = async (query: any) => {
    setLoading(true)
    try {
      const res: any = await queryTaskList({ ...filterQuery, page_size: pageSize,  ...query });
      const { code, msg } = res || {}
      if (code === 200) {
        setData(res)
      } else {
        message.error(msg || formatMessage({ id: 'request.data.failed'}) );
      }
      setLoading(false)
    } catch (e) {
      setLoading(false)
    }
  }

  // 分页点击
	const onChange = (page:number, pageSize:number) => {
    setPageSize(pageSize)
    getTableData({page_num: page, page_size: pageSize })
  }

  // 过滤查询
  const filterCallBack= (info: any) => {
    const query = { ...filterQuery, ...info }
    setFilterQuery(query)
    getTableData(query)
  }

  // 查看详情
  const detailCallBack = (info: any) => {
    detailsModal.current?.show({ item: info });
  }

	const onRow = (record: any) => { }

	return (
    <PageContainer>

      <PageCard boxShadow>
        <div>
          <h3><FormattedMessage id="task.list" /></h3>
          <TaskList
            data={data}
            onChange={onChange}
            onRow={onRow}
            loading={loading}
            filterCallBack={filterCallBack}
            detailCallBack={detailCallBack}
          />

          <DetailsModal ref={detailsModal}/>
        </div>
      </PageCard>
    </PageContainer>
	);
};

export default List;