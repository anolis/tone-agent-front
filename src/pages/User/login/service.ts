import { request } from 'umi';


// 1.用户登录
export const login = async (data: any) => {
  return request('/v1/account/login' , { 
    method : 'post' , data })
}

// 2.用户登出
export async function logout() {
  return request(`/v1/account/logout`, {
    method : 'post' })
}

// 3.用户信息
export async function queryUserInfo() {
  return request(`/v1/account/userinfo`, {
    method: 'GET',
    // params,
  });
}

