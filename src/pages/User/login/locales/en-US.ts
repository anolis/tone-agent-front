export default {
  'user.login.accountLogin.tab': 'Tone Agent',

  'user.login.username.placeholder': 'Please input',
  'user.login.username.required': 'Please input username!',
  'user.login.username.label': 'User Name',
  'user.login.password.label': 'Password',
  'user.login.password.placeholder': 'Please input',
  'user.login.password.required': 'Please input password!'
}
