import React, { useState, useEffect } from 'react';
import { Alert, Space, message, Tabs } from 'antd';
import { LockOutlined, UserOutlined, } from '@ant-design/icons';
import ProForm, { ProFormCaptcha, ProFormCheckbox, ProFormText } from '@ant-design/pro-form';
import { useIntl, Link, history, FormattedMessage, SelectLang, useModel } from 'umi';
import Footer from '@/components/Footer';
import { ReactComponent as Background } from '@/assets/svg/background.svg';
import { login } from './service';
import styles from './index.less';

const LoginMessage: React.FC<{
  content: string;
}> = ({ content }) => (
  <Alert
    style={{
      marginBottom: 24,
    }}
    message={content}
    type="error"
    showIcon
  />
);

/** 此方法会跳转到 redirect 参数所在的位置 */
const goto = () => {
  if (!history) return;
  setTimeout(() => {
    const { query } = history.location;
    const { redirect } = query as { redirect: string };
    history.push(redirect || '/');
  }, 10);
};

const Login: React.FC = () => {
  const [submitting, setSubmitting] = useState(false);
  const [userLoginState, setUserLoginState] = useState<API.LoginResult>({});
  const [type, setType] = useState<string>('account');
  const { initialState, setInitialState } = useModel('@@initialState');
  const intl = useIntl();

  // 监听当前页面宽度尺寸变化
  const [layoutHeight, setLayoutWidth] = useState(innerHeight)
  const getWindowWidth = () => setLayoutWidth(innerHeight)
  useEffect(() => {
      window.addEventListener('resize', getWindowWidth)
      return () => {
      window.removeEventListener('resize', getWindowWidth)
      }
  }, [])

  // 获取用户信息
  const fetchUserInfo = async () => {
    const userInfo = await initialState?.fetchUserInfo?.() || {};
    if (userInfo && userInfo.code && userInfo.code !== 200) {
      setUserLoginState({ type: 'error', message: userInfo.msg });
    }
    if (userInfo.username) {
      setInitialState({
        ...initialState,
        currentUser: userInfo,
      })
    }
  };

  // 登录
  const handleSubmit = async (values: API.LoginParams) => {
    setSubmitting(true);
    try {
      const res = await login({ ...values });
      if (res.code === 200) {
        // message.success('登录成功！');
        await fetchUserInfo();
        goto();
      } else {
        // 如果失败去设置用户错误信息
        setUserLoginState({ type: "error", message: res.msg });
      }
    } catch (error) {
      // 如果失败去设置用户错误信息
      setUserLoginState({ type: "error", });
    }
    setSubmitting(false);
  };

  // 重置用户登陆的提示状态
  const restLoginState = () => {
    if (userLoginState?.type === 'error') {
      setUserLoginState({})
    }
  }

  return (
    <div className={styles.container}>
      <div className={styles.lang}>{SelectLang && <SelectLang reload={false}
        postLocalesData={()=> [
          {lang: 'en-US', label: 'English', icon: '🇺🇸', title: 'Language' }, 
          {lang: 'zh-CN', label: '简体中文', icon: '🇨🇳', title: '语言' },
        ]}
      />}</div>
      <div className={styles.content} style={{ height: (layoutHeight - 40 - 94), }}>
        <div style={{ marginRight: 60 }}>
          <Background />
        </div>
        <div className={styles.main} >
          <ProForm
            initialValues={{
              autoLogin: true,
            }}
            submitter={{
              searchConfig: {
                submitText: intl.formatMessage({
                  id: 'pages.login.submit',
                  defaultMessage: '登录',
                }),
              },
              render: (_, dom) => dom.pop(),
              submitButtonProps: {
                loading: submitting,
                size: 'large',
                style: {
                  width: '100%',
                },
              },
            }}
            onFinish={async (values) => {
              handleSubmit(values as API.LoginParams);
            }}
          >
            <Tabs activeKey={type} onChange={setType} size={'large'}>
              <Tabs.TabPane
                key="account"
                tab={intl.formatMessage({
                  id: 'user.login.accountLogin.tab',
                  defaultMessage: 'Tone Agent',
                })}
              />
            </Tabs>

              {userLoginState.type === 'error' && (
                <LoginMessage content={userLoginState.message || '账户或密码错误'} />
              )}
              <>
                <div className={styles.label}><FormattedMessage id={'user.login.username.label'} /></div>
                <ProFormText
                  name="username"
                  fieldProps={{
                    size: 'large',
                    prefix: <UserOutlined className={styles.prefixIcon} />,
                    onChange: (e)=> {
                      restLoginState()
                    },
                  }}
                  placeholder={intl.formatMessage({
                    id: 'user.login.username.placeholder',
                    defaultMessage: '请输入',
                  })}
                  rules={[
                    {
                      required: true,
                      message: (
                        <FormattedMessage
                          id="user.login.username.required"
                          defaultMessage="请输入用户名!"
                        />
                      ),
                    },
                  ]}
                />
                <div className={styles.label}><FormattedMessage id={'user.login.password.label'} /></div>
                <ProFormText.Password
                  name="password"
                  fieldProps={{
                    size: 'large',
                    prefix: <LockOutlined className={styles.prefixIcon} />,
                    onChange: (e)=> {
                      restLoginState()
                    },
                  }}
                  placeholder={intl.formatMessage({
                    id: 'user.login.password.placeholder',
                    defaultMessage: '密码',
                  })}
                  rules={[
                    {
                      required: true,
                      message: (
                        <FormattedMessage
                          id="user.login.password.required"
                          defaultMessage="请输入密码！"
                        />
                      ),
                    },
                  ]}
                />
              </>
          </ProForm>
          

        </div>
      </div>

      <Footer />
    </div>
  );
};

export default Login;
