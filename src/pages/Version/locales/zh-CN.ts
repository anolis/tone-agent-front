export default {
  'version.list': '版本列表',

  'version.table.Version': '版本',
  'version.table.Arch': 'Arch',
  'version.table.Link': '链接',
  'version.table.ReleaseTime': '发布时间',
  'version.table.Description': '描述',
  //
  'version.modal.add.title': '新增版本', 
  'version.modal.edit.title': '编辑版本',
  'version.modal.Link.message': '请输入Link，允许最长{max}个字符',
  'version.modal.release_time.message': '选择时间',
}