export default {
  'version.list': 'Version List',

  'version.table.Version': 'Version',
  'version.table.Arch': 'Arch',
  'version.table.Link': 'Link',
  'version.table.ReleaseTime': 'Release time',
  'version.table.Description': 'Description',
  //
  'version.modal.add.title': 'Add Version',
  'version.modal.edit.title': 'Edit Version',
  'version.modal.Link.message': 'Please enter Link, maximum {max} characters allowed',
  'version.modal.release_time.message': 'Please select',
}