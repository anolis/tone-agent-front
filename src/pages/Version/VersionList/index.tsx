import React, { useState, useEffect } from 'react';
import { Popover, Tooltip, Space, Popconfirm } from 'antd';
import { FilterFilled, CopyOutlined } from '@ant-design/icons';
import { FormattedMessage, useIntl } from 'umi';
import CommonTable from '@/components/Public/CommonTable';
import PopoverEllipsis from '@/components/Public/PopoverEllipsis';
import SearchInput from '@/components/Public/SearchInput';
import Clipboard from 'clipboard'
import { getLimitAuthority } from '@/utils/utilsHelp';
import SelectStatus from '@/components/Public/SelectStatus';
import styles from './style.less';

const ProductList = ({ data, loading, onChange, filterCallBack, editCallBack= ()=> {}, onRow= ()=> {} }:any) => {
  const { formatMessage } = useIntl();
	const [autoFocus, setFocus] = useState<boolean>(true);
	const [version, setVersion] = useState<string>();
	const [arch, setArch] = useState<string>();
	// 权限
	const limitAuthority = getLimitAuthority();

	let columns: any = [
    {
			title: <FormattedMessage id="version.table.Version" />,
			dataIndex: 'version',
			width: 150,
			render: (text: any, record: any) => {
				const { is_new, is_stable } = record
				return (
					<div>
				    <PopoverEllipsis title={<span className={is_new? styles.new_icon: is_stable? styles.stable_icon: null}>{text}</span>} width={150} />
					</div>
				)
			},
			filterIcon: () => <FilterFilled style={{ color: version ? '#1890ff' : undefined }} />,
			filterDropdown: ({ confirm }: any) => <SearchInput confirm={confirm} autoFocus={autoFocus} onConfirm={(val: string) => { setVersion(val) }} />,
			onFilterDropdownVisibleChange: (visible: any) => {
				if (visible) {
						setFocus(!autoFocus)
				}
		  },
		},
		{
			title: 'OS',
			dataIndex: 'os',
			onCell: () => ({ style: { width: 90 } }),
			render: (text: any) => {
				return <span>{text || '-'}</span>
			}
		},
    {
			title: <FormattedMessage id="version.table.Arch" />,
			dataIndex: 'arch',
			onCell: () => ({ style: { maxWidth: 100 } }),
			render: (text: any) => {
				return <span>{text || '-'}</span>
			},
			filterIcon: () => <FilterFilled style={{ color: arch ? '#1890ff' : undefined }} />,
			filterDropdown: ({ confirm }: any) => <SearchInput confirm={confirm} autoFocus={autoFocus} onConfirm={(val: string) => { setArch(val) }} />,
			onFilterDropdownVisibleChange: (visible: any) => {
				if (visible) {
						setFocus(!autoFocus)
				}
		  },

		},
    {
			title: <FormattedMessage id="version.table.Link" />,
			dataIndex: 'link',
			render: (text: any) => {
				return <PopoverEllipsis title={text} width={180}>
					<a href={text}>{text}</a>
					{text &&
						<CopyOutlined data-clipboard-text={text} className={"copy_link_icon"}
						  style={{ cursor: 'pointer', position: 'absolute', right: 8, top: 12, color: '#1890ff'}}
						/>
					}
				</PopoverEllipsis>
			}
		},
		{
			title: <FormattedMessage id="version.table.ReleaseTime" />,
			dataIndex: 'release_time',
			width: 170,
      render: (text: any) => {
				return <PopoverEllipsis title={text} width={170} />
			}
		},
		{
			title: <FormattedMessage id="version.table.Description" />,
			dataIndex: 'description',
			width: 280,
      render: (text: any) => {
				return <PopoverEllipsis title={text} width={280} />
			}
		},
	]
	if (limitAuthority) {
    columns = columns.concat([
		{
      title: <FormattedMessage id="Table.columns.operation" />,
      onCell: () => ({ style: { minWidth: 100 } }),
      align: 'center',
      render: (text: any, record: any) => {
          return (
					  <div>
							<Space>
							  <a><span onClick={()=> editCallBack('edit', record)}><FormattedMessage id="operation.edit" /></span></a>
								<Popconfirm
									placement="bottomRight"
									title={formatMessage({ id: 'confirm.content.delete' })}
									onConfirm={()=> editCallBack('delete', record)}
									okText={formatMessage({ id: 'btn.ok' })}
									cancelText={formatMessage({ id: 'btn.close' })}
								>
									<a><span><FormattedMessage id="operation.delete" /></span></a>
                </Popconfirm>
							</Space>
            </div>
          )
      },
    },	
		])
	}
	useEffect(() => {
		// 复制
		const clipboardKernel = new Clipboard('.copy_link_icon')
		clipboardKernel.on('success', function(e) {
			e.clearSelection();
		})
		return () => {
			clipboardKernel.destroy()
		}
},[])


	useEffect(() => {
		const params = { page_num: 1, version, arch }
		filterCallBack(params)
  }, [version, arch]);

	let list = data.data, total = data.total, current = data.page_num, pageSize = data.page_size
	return <CommonTable
		columns={columns}
		total={total}
		pageSize={pageSize}
		current={current}
		list={list}
		loading={loading}
		onRow={onRow}
		onChange={onChange}
	/>
};

export default ProductList;