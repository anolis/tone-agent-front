
import React, { useState, forwardRef, useImperativeHandle } from 'react';
import { FormattedMessage, useIntl } from 'umi';
import moment from 'moment';
import { Modal, message, Form, Select, Input, DatePicker, Checkbox } from 'antd'
import CodeEditer from '@/components/Public/CodeEditer';
import { archData } from '@/utils/utilsHelp';
import { versionAdd, versionEdit } from '../service'
import styles from './AddModal.less'

/**
 * 新增机器
 */
export default forwardRef((props: any, ref: any) => {
  const [form] = Form.useForm();
  const [loading, setLoading] = useState<any>(false)
  const [visible, setVisible] = useState(false);
  const [data, setData] = useState<any>({});
  // const [title, setTitle] = useState('');
  // const [current, setCurrent] = useState<any>({})
  // console.log('data:', data)

  const { formatMessage } = useIntl();
  const placeholder = formatMessage({ id: "Form.select.placeholder" });
  const requiredMessage = formatMessage({ id: 'Form.select.message'});

  // 请求数据
  const getTableData = async (query: any) => {
    setLoading(true)
    try {
      const res: any = {} // await queryTaskList(query);
      const { code, msg } = res || {}
      if (code === 200) {
        setData(res)
      } else {
        message.error(msg || formatMessage({id: 'request.data.failed'}));
      }
      setLoading(false)
    } catch (e) {
      setLoading(false)
    }
  }

  useImperativeHandle(
    ref,
    () => ({
        show: ({ detailData }: any) => {
          setVisible(true)
          setData(detailData)
          // 表单数据回填
          const { release_time, ...others } = detailData || {}
          // console.log('others:', release_time, others)
          form.setFieldsValue({ ...others, release_time: release_time ? moment(release_time) : undefined })
        }
    })
  )

  const handleOk = () => {
    form.validateFields().then(async (values) => {
      setLoading(true);
      const param = {
        ...values,
        release_time: values.release_time ? values.release_time.format("YYYY-MM-DD HH:mm:ss") : undefined,
      }
      // console.log('param:', param )

      const { code, msg } = values.id ? await versionEdit(param) : await versionAdd(param);
      if (code === 200) {
        message.success(values.id? formatMessage({id: 'modified.succeeded'}): formatMessage({id: 'created.succeeded'}));
        setVisible(false); 
        form.resetFields();
        props.callback();
      } 
      else {
        message.error(msg || formatMessage({id: 'created.failed'}));
      }
      setLoading(false);
    }).catch(() => {
      setLoading(false);
    });
  };

  const handleCancel = () => {
    setVisible(false);
    form.resetFields();
  };

  // -----------start 时间 -----------
  function disabledDate(current: any) {
    const cur = moment(current).endOf('day') 
    const delayTime = moment().unix()
    return current && (cur < moment(delayTime).endOf('day'))
  }
  // 限制时分秒
  function disabledDateTime(date: any) {
    return {
      disabledHours: () => [],
      disabledMinutes: () => [],
      disabledSeconds: () => [],
    }
  }
  // -----------end 时间---------------

	return (
      <div>
        <Modal title={formatMessage({ id: (data.id? 'version.modal.edit.title': 'version.modal.add.title') }) }
          visible={visible}
          maskClosable={false}
          width={460}
          confirmLoading={loading}
          onOk={handleOk}
          onCancel={handleCancel}
        >
          <div>
          <Form form={form} layout="vertical">
            <Form.Item style={{ display: 'none'}}
              name="id">
              <Input type="hidden"/>
            </Form.Item>

            <Form.Item label="Version"
              name="version"
              rules={[
                {
                  required: true,
                  message: formatMessage({ id: "Form.input.message" }),
                  pattern: /^[A-Za-z0-9\._-]{1,50}$/g
                }
              ]}
            >
              <Input placeholder={formatMessage({ id: "Form.input.placeholder" })} />
            </Form.Item>

            <Form.Item label="OS"
              name="os"
              rules={[{
                required: true,
                message: requiredMessage,
              }]}>
              <Select placeholder={placeholder}>
                { 
                 ['Linux', 'Debian'].map((item: any) => (
                  <Select.Option key={item} value={item}>{item}</Select.Option>
                ))}
              </Select>
            </Form.Item>

            <Form.Item label="Arch"
              name="arch"
              rules={[{
                required: true,
                message: requiredMessage,
              }]}>
              <Select placeholder={placeholder}>
                { 
                 archData.map((item: any) => (
                  <Select.Option key={item.value} value={item.value}>{item.value}</Select.Option>
                ))}
              </Select>
            </Form.Item>

            <Form.Item label="Link"
              name="link"
              rules={[{
                required: true,
                max: 200,
                message: formatMessage({ id: "version.modal.Link.message" }, { max: 200 }),
              }]}>
                <Input placeholder={formatMessage({ id: "Form.input.placeholder" })}/>
            </Form.Item>

            <Form.Item label="Release time"
              name="release_time">
              <DatePicker showTime style={{ width: '100%' }} placeholder={formatMessage({ id: "version.modal.release_time.message" })}
                // disabledDate={disabledDate}
                // disabledTime={disabledDateTime}
                format="YYYY-MM-DD HH:mm:ss"
              />
            </Form.Item>

            <Form.Item label={<FormattedMessage id="agent.modal.Description" />}
              name="description">
              <Input.TextArea rows={4} placeholder={formatMessage({ id: "Form.input.placeholder" })} />
            </Form.Item>

            <Form.Item name="is_new" valuePropName="checked" style={{ marginBottom: 0}}>
              <Checkbox>最新版</Checkbox>
            </Form.Item>
            <Form.Item name="is_stable" valuePropName="checked" style={{ marginBottom: 0}}>
              <Checkbox>稳定版</Checkbox>
            </Form.Item>
          </Form>

          </div>
        </Modal>
      </div>
	);
});