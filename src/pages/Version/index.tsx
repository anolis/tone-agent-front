import { FormattedMessage, useIntl, history } from 'umi';
import React, { useCallback, useState, useEffect, useRef } from 'react';
import { message, Space, Button } from 'antd'
import { PageContainer, PageCard } from '@/components/Public'
import { getLimitAuthority } from '@/utils/utilsHelp';
import Table from './VersionList';
import AddModal from './Module/AddModal';
import { queryList, versionDelete } from './service'
import styles from './index.less'

const List = (props: any) => {
  const { formatMessage } = useIntl()
  const [loading, setLoading] = useState<any>(false)
  const [data, setData] = useState<any>({ data: [], total: 0, page: 1, page_size: 20 })
  const [filterQuery, setFilterQuery] = useState<any>({})
  const addModal: any = useRef(null)
  // 权限
	const limitAuthority = getLimitAuthority();

  // 1.请求数据
  const getTableData = async (query: any) => {
    setLoading(true)
    try {
      const res: any = await queryList({ ...filterQuery, page_size: data.page_size, ...query });
      const { code, msg } = res || {}
      if (code === 200) {
        setData(res)
      } else {
        message.error(msg || formatMessage({ id: 'request.data.failed'}) );
      }
      setLoading(false)
    } catch (e) {
      setLoading(false)
    }
  }

	useEffect(() => {
    // getTableData({ page_num: 1, page_size: 15 })
	}, [])

	const onChange = (page:number, pageSize:number) => {
    // console.log('onChange:')
    getTableData({ page_num: page, page_size: pageSize })
  }

  // 新增/修改/删除
  const addOrEditCallBack = (type: string, info: any) => {
    // console.log('data:', type, info)
    if (type === 'add') {
      addModal.current?.show({ detailData: {} });
    } else if (type === 'edit') {
      addModal.current?.show({ detailData: info });
    } else if (type === 'delete') {
      deleteClick(info)
    }
  }

  // 新增回调
  const addCallback = ()=> {
    getTableData({ page_num: data.page_num, page_size: data.page_size })
  }
  
  // 删除
  const deleteClick = async (query: any) => {
    const res = await versionDelete(query);
    if (res.code === 200) {
      message.success(formatMessage({ id: 'deletion.succeeded'}) );
      getTableData({ page_num: data.page_num, page_size: data.page_size })
    } else {
      message.error(res.msg || formatMessage({ id: 'request.data.failed'}) );
    }
  }

  // 过滤查询
  const filterCallBack= (info: any) => {
    const query = { ...info }
    setFilterQuery(query)
    getTableData(query)
  }

	return (
    <PageContainer>

    <PageCard>
      <div>
        <div className={styles.header}>
          <span className={styles.title}><FormattedMessage id="version.list" /></span>
          {limitAuthority ?
            <Space>
              <Button onClick={()=> addOrEditCallBack('add', {})} type="primary"><FormattedMessage id="version.modal.add.title" /></Button>
            </Space>
            : null
          }
        </div>

        <Table
          data={data}
          onChange={onChange}
          // onRow={onRow}
          loading={loading}
          filterCallBack={filterCallBack}
          editCallBack={addOrEditCallBack}
        />

        <AddModal ref={addModal} callback={addCallback} />
      </div>
      </PageCard>
    </PageContainer>
	);
};

export default List;
