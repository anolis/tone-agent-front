import { request } from 'umi';

// 1. 管理
export async function queryList(
  params: {},
  options?: { [key: string]: any },
) {
  return request<API.RuleList>('/v1/agent/version/manage', {
    method: 'GET',
    params: {
      ...params,
    },
    ...(options || {}),
  });
}

// 2.添加版本
export const versionAdd = async (data: any) => {
  return request('/v1/agent/version/manage' , { 
    method : 'post',
    data,
  })
}

// 3.修改版本
export async function versionEdit(data: any) {
  return request(`/v1/agent/version/manage`, {
    method: 'PUT',
    data,
  });
}

// 4. 删除
export async function versionDelete(params: any) {
  const { id } = params
  return request(`/v1/agent/version/manage`, {
    method: 'DELETE',
    params: {
      id,
    },
  });
}
