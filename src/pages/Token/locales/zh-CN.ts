export default {
  'Token.list': 'Token列表',

  'Token.table.Name': '名称',
  'Token.table.AccessKey': 'Access Key',
  'Token.table.SecretKey': 'Secret Key',
  'Token.table.AppName': 'App名称',
  'Token.table.Description': '描述',
  //
  'token.modal.add.title': '新增Token',
  'token.modal.edit.title': '编辑Token',
}