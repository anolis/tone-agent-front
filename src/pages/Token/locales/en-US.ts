export default {
  'Token.list': 'Token List',

  'Token.table.Name': 'Name',
  'Token.table.AccessKey': 'Access Key',
  'Token.table.SecretKey': 'Secret Key',
  'Token.table.AppName': 'App Name',
  'Token.table.Description': 'Description',
  //
  'token.modal.add.title': 'Add Token',
  'token.modal.edit.title': 'Edit Token',
}