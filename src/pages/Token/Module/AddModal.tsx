
import React, { useCallback, useState, useEffect, forwardRef, useImperativeHandle } from 'react';
import { FormattedMessage, history, useIntl } from 'umi';
import moment from 'moment';
import { Modal, message, Form, Select, Input, DatePicker, } from 'antd'
import CodeEditer from '@/components/Public/CodeEditer';
import { matchStatus, isUrl } from '@/utils/utilsHelp';
import { tokenAdd, tokenEdit } from '../service'
import styles from './AddModal.less'

/**
 * 新增机器
 */
export default forwardRef((props: any, ref: any) => {
  const [form] = Form.useForm();
  const [loading, setLoading] = useState<any>(false)
  const [visible, setVisible] = useState(false);
  const [data, setData] = useState<any>({});
  const [title, setTitle] = useState('');
  const [current, setCurrent] = useState<any>({})
  // console.log('data:', data)

  // 请求数据
  const getTableData = async (query: any) => {
    setLoading(true)
    try {
      const res: any = {} // await queryTaskList(query);
      const { code, msg } = res || {}
      if (code === 200) {
        setData(res)
      } else {
        message.error(msg || formatMessage({ id: 'request.data.failed'}) );
      }
      setLoading(false)
    } catch (e) {
      setLoading(false)
    }
  }

  useImperativeHandle(
    ref,
    () => ({
        show: ({ title= '', detailData}: any) => {
          setVisible(true)
          setTitle(title);
          setData(detailData)
          // 表单数据回填
          const { release_time, ...others } = detailData || {}
          // console.log('others:', release_time, others)
          form.setFieldsValue({ ...others, release_time: release_time ? moment(release_time) : undefined })
        }
    })
  )

  const handleOk = () => {
    form.validateFields().then(async (values) => {
      setLoading(true);
      const param = {
        ...values,
      }

      const { code, msg } = values.id ? await tokenEdit(param) : await tokenAdd(param);
      if (code === 200) {
        message.success(values.id? formatMessage({id: 'modified.succeeded'}): formatMessage({id: 'created.succeeded'}));
        setVisible(false); 
        form.resetFields();
        props.callback();
      } 
      else {
        message.error(msg || formatMessage({id: 'created.failed'}) );
      }
      setLoading(false);
    }).catch(() => {
      setLoading(false);
    });
  };

  const handleCancel = () => {
    setVisible(false);
    form.resetFields();
  };

  const { formatMessage } = useIntl();
  const placeholder = formatMessage({ id: "Form.select.placeholder" });
  const requiredMessage = formatMessage({ id: 'Form.select.message'});

	return (
      <div>
        <Modal title={formatMessage({ id: (data.id ? 'token.modal.edit.title' : 'token.modal.add.title') }) }
          visible={visible}
          maskClosable={false}
          width={460}
          confirmLoading={loading}
          onOk={handleOk}
          onCancel={handleCancel}
        >
          <div>
          <Form form={form} layout="vertical">
            <Form.Item style={{ display: 'none'}}
              name="id">
              <Input type="hidden"/>
            </Form.Item>

            <Form.Item label="Name"
              name="name"
              rules={[
                {
                  required: true,
                  message: formatMessage({ id: "Form.input.message" }),
                  pattern: /^[A-Za-z0-9\._-]{1,50}$/g
                }
              ]}
            >
              <Input placeholder={formatMessage({ id: "Form.input.placeholder" })} />
            </Form.Item>

            <Form.Item label="App Name"
              name="app_name"
              rules={[
                {
                  required: true,
                  message: formatMessage({ id: "Form.input.message" }),
                  pattern: /^[A-Za-z0-9\._-]{1,50}$/g
                }
              ]}
            >
              <Input placeholder={formatMessage({ id: "Form.input.placeholder" })} />
            </Form.Item>

            <Form.Item label={<FormattedMessage id="agent.modal.Description" />}
              name="description">
              <Input.TextArea rows={4} placeholder={formatMessage({ id: "Form.input.placeholder" })} />
            </Form.Item>
          </Form>

          </div>
        </Modal>
      </div>
	);
});