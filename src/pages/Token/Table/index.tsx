import React, { useState, useEffect } from 'react';
import { Popover, Tooltip, Space, Popconfirm } from 'antd';
import { QuestionCircleOutlined, FilterFilled, EyeInvisibleOutlined, EyeOutlined  } from '@ant-design/icons'
import { FormattedMessage, useIntl } from 'umi';
import CommonTable from '@/components/Public/CommonTable';
import PopoverEllipsis from '@/components/Public/PopoverEllipsis';
import SearchInput from '@/components/Public/SearchInput';
import SelectStatus from '@/components/Public/SelectStatus';
import styles from './style.less';

const ProductList = ({ data, loading, onChange, onRow, filterCallBack, editCallBack= ()=> {}, }:any) => {
	const { formatMessage } = useIntl();
	const [dataSource, setDataSource] = useState<any>([]);
	//
	const [autoFocus, setFocus] = useState<boolean>(true);
	const [name, setName] = useState<string>();
	const [access_key, setAccessKey] = useState<string>();
	const [secret_key, setSecretKey] = useState<string>();
	const [app_name, setAppName] = useState<string>();


	// 查看/隐藏
	const viewClick = (record: any) => {
		return (event: any) => {
			event.stopPropagation(); 
			const { id, showToken } = record;
			const temp = dataSource.map((item: any)=> item.id === id ? ({...item, showToken: !showToken }) : item )
			setDataSource(temp);
		}
	}

	const columns: any = [
    {
			title: <FormattedMessage id="Token.table.Name" />,
			dataIndex: 'name',
			render: (text:any) => {
				return <PopoverEllipsis title={text} width={105} />
			},
			filterIcon: () => <FilterFilled style={{ color: name ? '#1890ff' : undefined }} />,
			filterDropdown: ({ confirm }: any) => <SearchInput confirm={confirm} autoFocus={autoFocus} onConfirm={(val: string) => { setName(val) }} />,
			onFilterDropdownVisibleChange: (visible: any) => {
				if (visible) {
						setFocus(!autoFocus)
				}
		  },

		},
    {
			title: <FormattedMessage id="Token.table.AccessKey" />,
			dataIndex: 'access_key',
			render: (text:any) => {
				return <PopoverEllipsis title={text} width={265} />
			},
			filterIcon: () => <FilterFilled style={{ color: access_key ? '#1890ff' : undefined }} />,
			filterDropdown: ({ confirm }: any) => <SearchInput confirm={confirm} autoFocus={autoFocus} onConfirm={(val: string) => { setAccessKey(val) }} />,
			onFilterDropdownVisibleChange: (visible: any) => {
				if (visible) {
						setFocus(!autoFocus)
				}
		  },
			
		},
    {
			title: <FormattedMessage id="Token.table.SecretKey" />,
			dataIndex: 'secret_key',
			render: (text:any, record: any) => {
				// return <PopoverEllipsis title={text} width={300} />
				return (
					<>
          {record.showToken ? (
							<div className={styles.token}>
								<PopoverEllipsis title={text} width={300} />
								<span onClick={viewClick(record)}><EyeInvisibleOutlined style={{ color: '#1890ff' }}/></span>
							</div>
            ) : (
              <div className={styles.token}>
                <div className={styles.token_ellipsis}>{'**********************************************'}</div>
								<span onClick={viewClick(record)}><EyeOutlined /></span>
              </div>
            )
          }
        </>
				)
			},
			filterIcon: () => <FilterFilled style={{ color: secret_key ? '#1890ff' : undefined }} />,
			filterDropdown: ({ confirm }: any) => <SearchInput confirm={confirm} autoFocus={autoFocus} onConfirm={(val: string) => { setSecretKey(val) }} />,
			onFilterDropdownVisibleChange: (visible: any) => {
				if (visible) {
						setFocus(!autoFocus)
				}
		  },
			
		},
		{
			title: <FormattedMessage id="Token.table.AppName" />,
			dataIndex: 'app_name',
      render: (text:any) => {
				return <PopoverEllipsis title={text} width={100} />
			},
			filterIcon: () => <FilterFilled style={{ color: app_name ? '#1890ff' : undefined }} />,
			filterDropdown: ({ confirm }: any) => <SearchInput confirm={confirm} autoFocus={autoFocus} onConfirm={(val: string) => { setAppName(val) }} />,
			onFilterDropdownVisibleChange: (visible: any) => {
				if (visible) {
						setFocus(!autoFocus)
				}
		  },
			
		},
		{
			title: <FormattedMessage id="Token.table.Description" />,
			dataIndex: 'description',
      render: (text:any) => {
				return <PopoverEllipsis title={text} width={100} />
			}
		},

    {
      title: <FormattedMessage id="Table.columns.operation" />,
      onCell: () => ({ style: { minWidth: 100 } }),
      align: 'center',
      render: (text: any, record: any) => {
          return (<span>
							<Space>
								<a><span onClick={()=> editCallBack('edit', record)}><FormattedMessage id="operation.edit" /></span></a>
								<a><span onClick={()=> editCallBack('reset', record)}><FormattedMessage id="operation.reset" /></span></a>
								<Popconfirm
									placement="bottomRight"
									title={formatMessage({ id: 'confirm.content.delete' })}
									onConfirm={()=> editCallBack('delete', record)}
									okText={formatMessage({ id: 'btn.ok' })}
									cancelText={formatMessage({ id: 'btn.close' })}
								>
									<a><span><FormattedMessage id="operation.delete" /></span></a>
                </Popconfirm>
							</Space>
            </span>
          )
      },
    },
	]

	useEffect(() => {
		setDataSource(data.data)
  }, [data.data]);

	useEffect(() => {
		const params = { page_num: 1, name, access_key, secret_key, app_name }
		filterCallBack(params)
  }, [name, access_key, secret_key, app_name]);

	let total = data.total, current = data.page_num, pageSize = data.page_size;

	return <CommonTable
		columns={columns}
		total={total}
		pageSize={pageSize}
		current={current}
		list={dataSource}
		loading={loading}
		onRow={onRow}
		onChange={onChange}
	/>
};

export default ProductList;