import { request } from 'umi';

// 1. 管理
export async function queryList(
  params: {},
  options?: { [key: string]: any },
) {
  return request<API.RuleList>('/v1/account/manage', {
    method: 'GET',
    params: {
      ...params,
    },
    ...(options || {}),
  });
}

// 2.添加
export const tokenAdd = async (data: any) => {
  return request('/v1/account/manage' , { 
    method : 'post' , data })
}

// 3.修改
export async function tokenEdit(data: any) {
  return request(`/v1/account/manage`, {
    method: 'PUT',
    data,
  });
}

// 4.重置账号
export async function tokenReset(params: any) {
  const { id } = params
  return request(`/v1/account/token/reset`, {
    method: 'post',
    data: { id, },
  });
}

// 5. 删除
export async function tokenDelete(params: any) {
  const { id } = params
  return request(`/v1/account/manage`, {
    method: 'DELETE',
    params: { id, },
  });
}
