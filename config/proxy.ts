/**
 * 在生产环境 代理是无法生效的，所以这里没有生产环境的配置
 * The agent cannot take effect in the production environment
 * so there is no configuration of the production environment
 * For details, please see
 * https://pro.ant.design/docs/deploy
 * https://webpack.docschina.org/configuration/dev-server/#devserverproxy
 */
export default {
  dev: {
    '/v1/': {
      target: 'https://tone-agent.alibaba.net/',
      changeOrigin: true,
      pathRewrite: { '^': '' }, // 如果不希望传递/api，则需要重写路径
      secure: false,
      https: true,
    },
  },
  pre: {
    '/api/': {
      target: 'your pre url',
      changeOrigin: true,
      pathRewrite: { '^': '' },
    },
  },
};
