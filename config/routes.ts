﻿
export default [
  {
    path: '/user',
    layout: false,
    routes: [
      {
        path: '/user',
        routes: [
          {
            name: 'login',
            path: '/user/login',
            component: './User/login',
          },
        ],
      },
    ],
  },

  // ------------------
  {
    name: 'task.manage',
    icon: 'TagsOutlined',
    path: '/task',
    component: './Task',
  },
  {
    name: 'agent.manage',
    icon: 'Database',
    path: '/agent',
    component: './Agent',
  },
  {
    name: 'version.manage',
    icon: 'FolderOpen',
    path: '/version',
    component: './Version',
  },
  {
    name: 'account.manage',
    icon: 'SketchOutlined',
    path: '/account',
    component: './Token',
    access: 'canAdmin',
  },
  // -------------------
  {
    path: '/admin',
    name: 'admin',
    icon: 'crown',
    component: './Admin',
    hideInMenu: true,
    access: 'canAdmin',
    routes: [
      {
        path: '/admin/sub-page',
        name: 'sub-page',
        icon: 'smile',
        component: './Welcome',
      },
    ],
  },
  {
    name: 'list.table-list',
    icon: 'table',
    path: '/list',
    component: './TableList',
    hideInMenu: true,
  },
  {
    path: '/',
    redirect: '/task',
  },
  {
    component: './404',
  },
];
